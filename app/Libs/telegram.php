<?php

use Exception;
use \App\Model\Sender as Sender;
use \App\Utils\Loging as Loging;
use \App\Utils\GetValue as GetValue;
use \App\Utils\Sequence as Sequence;

class telegram extends Loging {

	public $recipient;
	private $g_division;
	private $g_group_send;
	private $g_provider;
	private $g_send;
	private $p_send_config;
	private $data;
	private $databases;

	public function __construct ($data, $databases, $APP_LOG = '') {
		parent::__construct($APP_LOG);
		$exp 				= explode('\\', __CLASS__);
		$this->data			= $data;
		$this->databases	= $databases;
	}

	private function writeToLog ($prefix, $content) {
		$this->write(get_class($this)."::".$prefix, $content);
	}

	public function token_used () { return 1; }
	
	public function param_check () {
		$mandatory_param = array (
			"chat_id",
			"message",
			"provider"
		);
		foreach ($mandatory_param as $index) {
			if (!isset($this->data[$index])) {
				$this->writeToLog(__FUNCTION__, "$index NOT SET");
				return false;
			}
		}
		return true;
	}

	public function recipient_check () {
		$this->recipient = $this->data['chat_id'];
		return true;
	}

	public function set_psendconfig () {
		$result = $this->databases->query(
			'g_provider',
			"SELECT `id`, `name`, `sname`, `have_own_qsend` FROM `g_provider` ".
			"WHERE `type` = 'SOCIAL' AND `param` = '".get_class($this)."'"
		);
		if (!$result) {
			$this->writeToLog(__FUNCTION__, 
			"SELECT `id`, `name`, `sname`, `have_own_qsend` FROM `g_provider` ".
			"WHERE `type` = 'EMAIL'");
			return false;
		} 
		$result = mysqli_fetch_all($result, MYSQLI_ASSOC);
		if (count($result) > 0) {
			$this->g_provider = $result[0];
			$result = $this->databases->query(
				'g_send',
				"SELECT `id-p-s-cfg` FROM `g_send` ".
				"WHERE `id-gs` = {$this->g_division['id-g-send']} ".
				"AND `id-p` = {$this->g_provider['id']} "
			);
			if (!$result) {
				$this->writeToLog(__FUNCTION__,
				"SELECT `id-p-s-cfg` FROM `g_send` ".
				"WHERE `id-gs` = {$this->g_division['id-g-send']} ".
				"AND `id-p` = {$this->g_provider['id']} ");
				return false;
			} 
			$result = mysqli_fetch_all($result, MYSQLI_ASSOC);
			if (count($result) < 1) {
				$this->writeToLog(__FUNCTION__, "not found g_send");
				return false;
			}
			$this->g_send = $result[0];
			$result = $this->databases->query(
				'p_send_config',
				"SELECT `type_send`, `id`, `send_ip_no`, `send_port`, ".
				"`send_method`, `send_port_vers`, `send_port_model`, ".
				"`service_id_prvd`, `file_name`, `dlrurl`, `dlrmask`, ".
				"`parameter`, `send_mesg_format`, `send_username`, ".
				"`send_password`, `type_delivery`, `with_plus_msisdn`, ".
				"`shortnumber`, `message_send`, `message_statusdelivery`, ".
				"`moremessage_send`, `moremessage_statusdelivery`, ".
				"`getresp_statussend`, `getresp_statusdelivery`, ".
				"`statusdlvr_delivered`, `statusdlvr_undelivered`, ".
				"`dlvr_ip_no`, `dlvr_port`, `dlvr_method`, `dlvr_port_vers`, ".
				"`dlvr_port_model`, `dlvr_file_name`, `dlvr_parameter`, ".
				"`statusdlvr_pending`, `token`, `secret_token` FROM `p_send_config`  ".
				"WHERE `id` = {$this->g_send['id-p-s-cfg']}"
			);
			if (!$result) return false; 
			$result_psendconfig = mysqli_fetch_all($result, MYSQLI_ASSOC);
			if (count($result_psendconfig) > 0) {
				$this->p_send_config = $result_psendconfig[0];
			}
			else {
				$this->writeToLog(__FUNCTION__, "not found p-send_config");
				return false;
			}
		}
		else {
			$this->writeToLog(__FUNCTION__, "not found g_provider");
			return false;
		}
		return true;
	}

	public function get_g_corporate () {
		$result = $this->databases->query(
			'g_corporate',
			"SELECT `id`, `db_prefix` FROM `g_corporate` ".
			"WHERE `id` = {$this->g_division['id-c']}"
		);
		$result = mysqli_fetch_all($result, MYSQLI_ASSOC);
		if (count($result) > 0) {
			$this->g_corporate = $result[0];
			return true;
		}
		else {
			$this->writeToLog(__FUNCTION__, "g corporate not found");
			return false;
		}
	}

	public function get_routes () {
		$result = $this->databases->query(
			'routes',
			"SELECT `protocol`, `ip`, `port`, `method`, `path`, `timeout` FROM `routes` ".
			"WHERE `channel_type` = 'SOCIAL' ".
			"AND `id-g` = {$this->g_division['id-g-routes']}"
		);
		$result = mysqli_fetch_all($result, MYSQLI_ASSOC);
		if (count($result) > 0) {
			$this->routes = $result[0];
			return true;
		}
		else {
			$this->writeToLog(__FUNCTION__, "routes not found");
			return false;
		}
	}

	public function sendToCallback ($status = 99) {
		$this->writeToLog(__FUNCTION__, 'send to callback');
		$data_send = [
			"ref_id"		=> $this->ref_id,
			"code_sms"		=> $GLOBALS['code_sms'],
			"type_status"	=> "send",
			"status"		=> "$status",
			"channel"		=> get_class($this),
			"time_send"		=> time(),
			"time_prov_recv"=> time()
		];
		$data_dlvr = [
			"ref_id"		=> $this->ref_id,
			"code_sms"		=> $GLOBALS['code_sms'],
			"type_status"	=> "dlvr",
			"status"		=> "$status",
			"channel"		=> get_class($this),
			"time_dlvr"		=> time()
		];
		$thread = 1;
		if (isset($GLOBALS['APP_THREAD_TO_CALLBACK']))
			$thread = Sequence::getNextThreadSequence("__KEY_TOCALLBACK__", (int)$GLOBALS['APP_THREAD_TO_CALLBACK']);
		$this->databases->query(
			'360_to_callback', 
			"INSERT INTO `360_to_callback` (`url`, `data`, `thread`) VALUES ".
			"('{$this->g_division['url_callback']}', '".json_encode($data_send)."', $thread), ".
			"('{$this->g_division['url_callback']}', '".json_encode($data_dlvr)."', $thread)"
		);
	}

	public function sending ($ref_id, $g_division, $g_group_send, $request) {
		$this->ref_id		= $ref_id;
		$this->g_group_send	= $g_group_send;
		$this->g_division	= $g_division;
		$this->data			= $request['channel'][get_class($this)];
		$this->writeToLog(__FUNCTION__, "channel data = ".json_encode($this->data));

		if (!$this->set_psendconfig()) {
			$this->writeToLog(__FUNCTION__, "PSENDCONFIG ERROR");
			return ['rc' => 2];
		}
		if (!$this->get_g_corporate()) {
			$this->writeToLog(__FUNCTION__, "GCORPORATE ERROR");
			return ['rc' => 2];
		}
		if (!$this->get_routes()) {
			$this->writeToLog(__FUNCTION__, "ROUTES ERROR");
			return ['rc' => 2];
		}

		$client_name	= $this->g_corporate['db_prefix'];
		$compare 		= ['bbca', 'bbri', 'bnii'];
		if (!in_array($client_name, $compare)) $client_name = 'cidx';

		$GLOBALS["f7_tid"] = GetValue::GetF7TrxID($this->data['chat_id']);
		if (!isset($this->data['attachment'])) 	$this->data['attachment'] = "";
		if (!isset($this->data['location'])) 		$this->data['location'] = "";
		if (!isset($this->data['message_type'])) 	$this->data['message_type'] = "message";
		
		$data_sendtomq = [
			"channel"		=> get_class($this),
			"social_type" 	=> $this->data['message_type'],
			"chatid"		=> $this->data['chat_id'],
			"token" 		=> $this->p_send_config['token'],
			"secret_token" 	=> $this->p_send_config['secret_token'],
			"message_type" 	=> $this->data['message_type'],
			"location" 		=> $this->data['location'],
			"subject" 		=> $request['subject'],
			"message"		=> $this->data['message'],
			"attachment" 	=> $this->data['attachment'],
			"id-p-s-cfg"	=> $this->g_send['id-p-s-cfg'],
			"client_id" 	=> $this->g_corporate['id'],
			"client_name" 	=> $client_name,
			"division_id" 	=> $this->g_division['id'],
			"division_name" => $this->g_division['name'],
			"username" 		=> $this->p_send_config['send_username'],
			"sender"		=> $this->g_group_send['name'],
			"sending_type" 	=> $this->p_send_config['type_send'],
			"media"			=> "client",
			"ip_remote" 	=> $this->p_send_config['send_ip_no'],
			"code_sms"		=> $GLOBALS['code_sms'],
			"ref_id"		=> $ref_id,
			"sending_param"	=> serialize([
				'prvdName' 					=> $this->g_provider['name'],
				'name'						=> $this->g_provider['name'],
				'have_own_qsend'			=> $this->g_provider['have_own_qsend'],
				'id'						=> $this->p_send_config['id'],
				'send_ip_no'				=> $this->p_send_config['send_ip_no'],
				'send_port'					=> $this->p_send_config['send_port'],
				'send_method'				=> $this->p_send_config['send_method'],
				'send_port_vers'			=> $this->p_send_config['send_port_vers'],
				'send_port_model'			=> $this->p_send_config['send_port_model'],
				'service_id_prvd'			=> $this->p_send_config['service_id_prvd'],
				'file_name'					=> $this->p_send_config['file_name'],
				'dlrurl'					=> $this->p_send_config['dlrurl'],
				'dlrmask'					=> $this->p_send_config['dlrmask'],
				'parameter'					=> $this->p_send_config['parameter'],
				'send_mesg_format'			=> $this->p_send_config['send_mesg_format'],
				'send_username'				=> $this->p_send_config['send_username'],
				'send_password' 			=> $this->p_send_config['send_password'],
				'type_delivery'				=> $this->p_send_config['type_delivery'],
				'with_plus_msisdn'			=> $this->p_send_config['with_plus_msisdn'],
				'shortnumber'				=> $this->p_send_config['shortnumber'],
				'message_send'				=> $this->p_send_config['message_send'],
				'message_statusdelivery' 	=> $this->p_send_config['message_statusdelivery'],
				'moremessage_send'			=> $this->p_send_config['moremessage_send'],
				'moremessage_statusdelivery'=> $this->p_send_config['moremessage_statusdelivery'],
				'getresp_statussend'		=> $this->p_send_config['getresp_statussend'],
				'getresp_statusdelivery'	=> $this->p_send_config['getresp_statusdelivery'],
				'statusdlvr_delivered'		=> $this->p_send_config['statusdlvr_delivered'],
				'statusdlvr_undelivered'	=> $this->p_send_config['statusdlvr_undelivered'],
				'dlvr_ip_no'				=> $this->p_send_config['dlvr_ip_no'],
				'dlvr_port'					=> $this->p_send_config['dlvr_port'],
				'dlvr_method'				=> $this->p_send_config['dlvr_method'],
				'dlvr_port_vers'			=> $this->p_send_config['dlvr_port_vers'],
				'dlvr_port_model'			=> $this->p_send_config['dlvr_port_model'],
				'dlvr_file_name'			=> $this->p_send_config['dlvr_file_name'],
				'dlvr_parameter'			=> $this->p_send_config['dlvr_parameter'],
				'statusdlvr_pending'		=> $this->p_send_config['statusdlvr_pending'],
				'url_client_status_sent'	=> $this->g_division['url_sent'],
				'url_client_delivery_report'=> $this->g_division['url_delivery']
			]),
			"provider"		=> $this->g_provider['sname'],
			"servicetype"	=> $this->g_division['service_type'],
			"url_callback"	=> $this->g_division['url_callback'],
			"f7_tid"		=> $GLOBALS["f7_tid"]
		];
		
		try {
			$result = Sender::go($this->routes, $data_sendtomq);
			$this->writeToLog(__FUNCTION__, "result = ".json_encode($result));
			return $result;
		}
		catch (Exception $e) {
			$this->writeToLog(__FUNCTION__, 'failed when to send cgw sms');
			return ['rc' => 2];
		}

	}

}

?>
