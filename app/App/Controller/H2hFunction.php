<?php

    function getDataFromDB($array_req = array())
    {
      // require_once __DIR__ . '/../../Config/Connection_h2h.php';

      global $conn;

      $msisdn = substr($array_req['destination'], 0, 1);
      $conv_msisdn = $msisdn==0?"62".substr($array_req['destination'],1):$array_req['destination'];

      $prefix = substr($conv_msisdn, 0, 5);

      $sql_division = "SELECT username,`id-g-send`,password FROM g_division WHERE username = '".$array_req['user']."' AND password = '".$array_req['password']."'";
      $result_division = $conn->query($sql_division);
      $rs_division = $result_division->fetch_assoc();
      $check_division = count($rs_division);
        if ($check_division==0) {
          $res = array(
			      'rc' => '4',
            'status' => 'failed',
            'message' => 'no data in table',
            'data' => $array_req
          );
          print_r(json_encode($res));
          exit();
        }

      $sql_provider = "SELECT prov.name FROM p_prefix as prf JOIN g_provider as prov ON prf.`id-p`=prov.id WHERE prf.`number`='".$prefix."'";
      $result_prov = $conn->query($sql_provider);
      $rs_prov = $result_prov->fetch_assoc();
      $check_prov = count($rs_prov);
        if ($check_prov==0) {
          $res = array(
			      'rc' => '3',
            'status' => 'failed',
            'message' => 'no data in table',
            'data' => $array_req
          );
          print_r(json_encode($res));
          exit();
        }

      $conn->close();

      $data = array(
        'username' => $rs_division['username'],
        'password' => $rs_division['password'],
        'id-g-send' => $rs_division['id-g-send'],
        'name' => $rs_prov['name']
      );

      return $data;
    }

    function getDataFromDB2($array_req = array())
    {
      global $conn;

      $prefix = substr($array_req['recipient'], 0, 5);

      $sql_division = "SELECT username,`id-g-send`,password,corporate_id_sandeza,division_id_sandeza FROM g_division WHERE username = '".$array_req['user']."' AND id = ".$array_req['div_id'];
      $result_division = $conn->query($sql_division);
      $rs_division = $result_division->fetch_assoc();
      $check_division = count($rs_division);
        if ($check_division==0) {
          $res = array(
			      'rc' => '4',
            'status' => 'failed',
            'message' => 'no data in table',
            'data' => $array_req
          );
          print_r(json_encode($res));
          exit();
        }

      $sql_provider = "SELECT prov.name FROM p_prefix as prf JOIN g_provider as prov ON prf.`id-p`=prov.id WHERE prf.`number`='".$prefix."'";
      $result_prov = $conn->query($sql_provider);
      $rs_prov = $result_prov->fetch_assoc();
      $check_prov = count($rs_prov);
        if ($check_prov==0) {
          $res = array(
			      'rc' => '3',
            'status' => 'failed',
            'message' => 'no data in table',
            'data' => $array_req
          );
          print_r(json_encode($res));
          exit();
        }

      $data = array(
        'username' => $rs_division['username'],
        'password' => $rs_division['password'],
        'id-g-send' => $rs_division['id-g-send'],
        'corporate_id_sandeza' => $rs_division['corporate_id_sandeza'],
        'division_id_sandeza' => $rs_division['division_id_sandeza'],
        'recipient' => $array_req['recipient'],
        'channel' => $array_req['channel'],
        'provider' => $rs_prov['name'],
        'message' => $array_req['message'],
        'ref_id' => $array_req['ref_id'],
        'code_sms' => $array_req['code_sms'],
        'time_request' => $array_req['time_request'],
        'time_schedule' => $array_req['time_schedule']
      );

      return $data;
    }

    function GUID()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    function CheckParam($array_req = array(), $is_null){
      if ($is_null) {
      $field_required = array_search(null, $array_req);
        if (!empty($field_required)) {
          $res = array(
			      'rc' => '1',
            'status' => 'failed',
            'message' => 'field '.$field_required.' is required',
            'data' => $array_req
          );
          print_r(json_encode($res));
          exit();
        }
      }
      else {
        if (!isset($_GET["u"])) {
          $param = 'u';
        }
        if (!isset($_GET["p"])) {
          $param = 'p';
        }
        if (!isset($_GET["d"])) {
          $param = 'd';
        }
        if (!isset($_GET["m"])) {
          $param = 'm';
        }

          $res = array(
			      'rc' => '1',
            'status' => 'failed',
            'message' => 'field '.$param.' is required',
            'data' => $array_req
          );
          print_r(json_encode($res));
          exit();
      }
    }

    function CheckDataFromTbl(){
      global $conntrx;

      $sql_check = "SELECT * FROM `360_reporth2h_recv` LIMIT 0,100";
      // $sql_delete = "DELETE FROM `360_reporth2h_recv` WHERE `id` = ".$row['id']."";
      $result_check = $conntrx->query($sql_check);

        if ($result_check->num_rows > 0) {
          // output data of each row
          $data=array();
          while($row = $result_check->fetch_assoc()) {
              $conntrx->query("DELETE FROM `360_reporth2h_recv` WHERE `id` = ".$row['id']."");
              $data[] = array(
                'id' => $row['id'],
                'corp_id' => $row['corp_id'],
                'div_id' => $row['div_id'],
                'refnum' => $row['refnum'],
                'type' => $row['type'],
                'code_sms' => $row['code_sms'],
                'subject' => $row['subject'],
                'recipient' => $row['recipient'],
                'user' => $row['user'],
                'channel' => $row['channel'],
                'provider' => $row['provider'],
                'time_request' => $row['time_request'],
                'time_schedule' => $row['time_schedule'],
                'time_client' => $row['time_client'],
                'time_recv' => $row['time_recv'],
                'time_log' => $row['time_log'],
                'message' => $row['message']
              );
          }
              $dt = array(
              'data' => $data
              );
        } else {
            exit();
        }

        return $dt;
    }

    // function deleteDataH2h($id_data = array()){
    //   global $conntrx;
    //
    //   $impl = "(".implode(',',$id_data).")";
    //   $sql = "DELETE from `360_reporth2h_recv` WHERE id IN ".$impl;
    //
    //   $result = $conntrx->query($sql);
    //   return $result;
    // }

?>
