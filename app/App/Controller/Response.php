<?php

namespace App\Controller;
use \App\Utils\Loging;

class Response extends \App\Utils\Loging {

    public function __construct ($APP_LOG = '') {
        parent::__construct($APP_LOG);
    }

    public function SUCCESS () {
        $R = "0";
        $this->ENDED(__FUNCTION__, $R);
    }

    public function INVALID_PARAM () {
        $R = "1";
        $this->ENDED(__FUNCTION__, $R);
    }

    public function INTERNAL_ERROR () {
        $R = "2";
        $this->ENDED(__FUNCTION__, $R);
    }
    
    public function INVALID_RECIPIENT () {
        $R = "3";
        $this->ENDED(__FUNCTION__, $R);
    }

    public function INVALID_SIGN () {
        $R = "4";
        $this->ENDED(__FUNCTION__, $R);
    }

    public function INVALID_CORPORATE () {
        $R = "5";
        $this->ENDED(__FUNCTION__, $R);
    }

    public function NOT_IP_WHITELIST () {
        $R = "6";
        $this->ENDED(__FUNCTION__, $R);
    }

    public function NOT_ENOUGHT_TOKEN () {
        $R = "7";
        $this->ENDED(__FUNCTION__, $R);
    }

    public function UNKNOWN_SENDER_ID () {
        $R = "8";
        $this->ENDED(__FUNCTION__, $R);
    }

    public function INVALID_REFNUM () {
        $R = "9";
        $this->ENDED(__FUNCTION__, $R);
    }
    
    public function DOUBLE_DATA () {
        $R = "10";
        $this->ENDED(__FUNCTION__, $R);
    }

    private function ENDED ($STATUS, $R) {
        $ref_id = "";
        $code_sms = "";
        if (isset($GLOBALS["ref_id"])) $ref_id = $GLOBALS["ref_id"];
        if (isset($GLOBALS["code_sms"])) $code_sms = $GLOBALS["code_sms"];

        $response = json_encode(array (
            "rc"        => $R,
            "ref_id"    => $ref_id,
            "code_sms"  => $code_sms
        ));
        $this->write("RESPONSE", "$STATUS = $response");
        header('Content-Type: application/json');
        echo $response;
        //$end = microtime(true);
        //$time = ($end - $GLOBALS['start']) * 1000;
        //$this->write("TIME RESPONSE", (string)$time." ms");
        exit();
    }

}

?>
