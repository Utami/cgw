<?php

namespace App\Controller;
use \App\Controller\CgwFunction_dev as CgwFunction;
use \App\Controller\Response as Response;
use \App\Utils\Sequence as Sequence;
use \App\Utils\GetValue as GetValue;
use \App\Model\Databases as Databases;

class Request_dev extends CgwFunction_dev {
	protected $request;
	protected $response;
	protected $channel;
	protected $g_division;
	protected $g_group_send;
	protected $databases;
	protected $app_log;

	public function __construct ($request, $APP_LOG = '') {
		parent::__construct($APP_LOG);
		$this->app_log		= $APP_LOG;
		$this->request		= $request;
		$this->response		= new Response($APP_LOG);
		$this->databases	= new Databases;
	}
	
	public function run () {
		$this->write(__FUNCTION__, "REQUEST=".json_encode($this->request));
		if (!$this->request_check()){ $this->response->INVALID_PARAM(); }
		if (!$this->set_division())		$this->response->INVALID_CORPORATE();
		if (!$this->check_sender_id())	$this->response->UNKNOWN_SENDER_ID();
		if (!$this->sign_check())		$this->response->INVALID_SIGN();
		if (!$this->ip_check())			$this->response->NOT_IP_WHITELIST();
		if (!$this->set_sender())		$this->response->UNKNOWN_SENDER_ID();
		$GLOBALS['code_sms']	= GetValue::ReplyCode();
		$GLOBALS['ref_id']		= $this->request['ref_id'];
		if ((int)$this->request["type"] == 1)		$this->type_backup();
		
		/*
		if ((int)$this->request["type"] == 1)		$this->type_backup();
		else if ((int)$this->request["type"] == 2)	$this->type_sendall();
		
		$this->response->SUCCESS();*/
	}
	
	private function type_backup () {
		$this->write(__FUNCTION__, "processing backup");
		//=== START TOKEN
		$token_used = 0;
		foreach ($this->request['channel'] as $index => $data) {
			if (!$this->build_channel($index)) {
				$this->write(__FUNCTION__, "error build_channel $index");
				$this->response->INTERNAL_ERROR();
			}
			$c_token_used = $this->channel[$index]->token_used();
			if ($token_used < $c_token_used){ $token_used = $c_token_used; }
		}// end foreach channel
		if ($this->g_division['token_type'] == 't') {
			if ($this->token_used($token_used) < 1){ $this->response->NOT_ENOUGHT_TOKEN(); }
		}
		//=== END TOKEN
		$index = key($this->request['channel']);
		if (!$this->build_channel($index)) {
			$this->write(__FUNCTION__, "error build_channel $index");
			$this->response->INTERNAL_ERROR();
		}
		if (!$this->channel[$index]->param_check()) {
			$this->write(__FUNCTION__, "error param_check $index");
			$this->response->INVALID_PARAM();
		}
		if (!$this->channel[$index]->recipient_check())	{
			$this->write(__FUNCTION__, "error recipient_check $index");
			$this->response->INVALID_RECIPIENT();
		}
		$t_prefix = Sequence::getNextTrxSequence();
		/*$this->insert_to_temp((string)$t_prefix);
		$this->insert_to_recv(
			$index, 
			$this->channel[$index]->recipient,	
			$this->request['channel'][$index]['provider']
		);*/
		$result = $this->channel[$index]->sending (
			"{$this->request['ref_id']}_$t_prefix",
			$this->g_division,
			$this->g_group_send,
			$this->request
		);
		print_r("aa ".$t_prefix);
	}// end type_backup
}