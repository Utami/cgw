<?php

namespace App\Controller;
use \App\Controller\CgwFunction as CgwFunction;
use \App\Controller\Response as Response;
use \App\Utils\Sequence as Sequence;

class Cgw extends CgwFunction {

	protected $request;
	protected $response;
	protected $channel;
	protected $g_division;
	protected $g_group_send;

	protected $index_recipient = [
		'sms'		=> 'msisdn',
		'email'		=> 'email',
		'social' 	=> 'chat_id'
	];

    public function __construct ($request) {
		parent::__construct();
		$this->request		= $request;
		$this->response		= new Response;
	}

	public function execute_cgw () {
		$this->write(__FUNCTION__, "REQUEST=".json_encode($this->request));
		$this->param_cgw_check();

		$this->set_division();
		$this->sign_check();
		$this->whitelist_ip_check();
		$this->token_check();
		$this->set_sender();
		if ((int)$this->request["type"] == 1) $this->type_backup();
		else if ((int)$this->request["type"] == 2) $this->type_sendall();
		$this->response->SUCCESS();
	}

	public function execute_callback () {
		$this->write(__FUNCTION__, "REQUEST=".json_encode($this->request));
		$this->param_callback_check();
		if ($this->request['type_status'] == 'sent') $this->callback_processing();
		else if ($this->request['type_status'] == 'dlvr') $this->callback_client_dlvr();
		$this->response->SUCCESS();
	}

	private function type_sendall() {
		$this->write(__FUNCTION__, "processing");
		foreach ($this->request["channel"] as $index => $data) $this->recipient_check($index);
		foreach ($this->request["channel"] as $index => $data) {
			if ($index == 'social') {
				foreach ($this->request['channel'][$index] as $value) {
					$this->insert_to_recv(
						$index,
						$value[$this->index_recipient[$index]],
						$value['provider'],
						$value['social_id']
					);
					$result = $this->channel[$index]->sending(
						$this->request['ref_id'],
						$value,
						$this->g_division,
						$this->g_group_send,
						$this->request
					);
					if ((int)$result['rc'] != 0) {
						$this->channel[$index]->sendToCallback();
					}
				}
			}
			else {
				$this->insert_to_recv(
					$index,
					$data[$this->index_recipient[$index]],
					$data['provider']
				);
				$result = $this->channel[$index]->sending (
					$this->request["ref_id"],
					$data,
					$this->g_division,
					$this->g_group_send,
					$this->request
				);
				if ((int)$result['rc'] != 0) {
					$this->channel[$index]->sendToCallback();
				}
			}
		}
	}

	private function type_backup () {
		$this->write(__FUNCTION__, "processing");
		$index = key($this->request['channel']);
		$t_prefix = Sequence::getNextTrxSequence();
		$this->recipient_check($index);
		$this->insert_to_temp((string)$t_prefix);
		if ($index == 'social') {
			foreach ($this->request['channel'][$index] as $value) {
				$this->insert_to_recv(
					$index,
					$value[$this->index_recipient[$index]],
					$value['provider'],
					$value['social_id']
				);
				$result_channel = $this->channel[$index]->sending(
					$this->request['ref_id']."_$t_prefix",
					$value,
					$this->g_division,
					$this->g_group_send,
					$this->request
				);
				break;
			}
		}
		else {
			$this->insert_to_recv(
				$index,
				$this->request['channel'][$index][$this->index_recipient[$index]],
				$this->request['channel'][$index]['provider']
			);
			$result_channel = $this->channel[$index]->sending(
				$this->request['ref_id']."_$t_prefix",
				$this->request['channel'][$index],
				$this->g_division,
				$this->g_group_send,
				$this->request
			);
		}
		if ((int)$result_channel['rc'] != 0) {
			$this->channel[$index]->sendToCallback();
		}
	}

	private function callback_processing () {
		$GLOBALS["code_sms"] = $this->request["code_sms"];
		$GLOBALS["ref_id"] = $this->request["ref_id"];
		$is_backup_transaction = strpos($this->request["ref_id"], '_');
		if ($is_backup_transaction) {
			$this->write(__FUNCTION__, "backup");
			$exp_ref_id = explode("_", $this->request["ref_id"]);
			$t_prefix = $exp_ref_id[1];
			$this->request["ref_id"] = $exp_ref_id[0];
			$this->insert_to_send_with_checking($this->request['channel']);
			if ((int)$this->request["status"] != 0){
				$this->callback_backup_channel($t_prefix);
				$this->callback_client_sent($this->request_prev, $this->request);
			}
			else {
				$this->g_division = $this->get_url_client($this->recv[0]['div_id']);
				$this->request['provider'] = $this->recv[0]['provider'];
				$this->callback_client_sent($this->request, null);
				$this->delete_by_refnum_from_temp($this->request["ref_id"], $t_prefix);
			}
		}
		else {
			$this->insert_to_send_with_checking($this->request['channel']);
			//$this->insert_to_sendtoclient();
			$this->request['provider'] = $this->recv[0]['provider'];
			$this->g_division = $this->get_url_client($this->recv[0]['div_id']);
			$this->callback_client_sent($this->request, null);
		}
	}

	private function callback_backup_social ($channel_db, $result_temp) {
		$f_break	= false;
		$f_breaked	= false;
		foreach ($channel_db['social'] as $value) {
			if ($f_break) {
				$this->request = [
					'ch_now'	=> 'social',
					'social_id'	=> $value['social_id'],
					'provider'	=> $value['provider'],
					'message'	=> $value['message'],
					'recipient'	=> $value[$this->index_recipient['social']],
					'ref_id'	=> $this->request['ref_id'],
					'username'	=> $result_temp['user'],
					'type'		=> 1,
					'time'		=> strtotime($result_temp['time_client']),
					'time_request'	=> $result_temp['time_request'],
					'time_schedule'	=> $result_temp['time_schedule'],
					'subject'	=> $result_temp['subject'],
					'sender_id' => $result_temp['sender_id'],
					'channel'	=> [
						'social' => [
							$value
						]
					],
					'data'		=> $value
				];
				$f_breaked = true;
				break;
			}
			if ($this->request['social_id'] == $value['social_id']){
				$this->request_prev['provider'] = $value['provider'];
				$f_break = true;
			}
		}
		return $f_breaked;
	}

	private function callback_backup_type ($channel_db, $result_temp) {
		$f_break	= false;
		$f_breaked	= false;
		foreach ($channel_db as $type => $value) {
			if ($f_break) {
				$this->request = [
					'ch_now'	=> $type,
					'social_id'	=> '',
					'ref_id'	=> $this->request['ref_id'],
					'username'	=> $result_temp['user'],
					'type'		=> 1,
					'time'		=> strtotime($result_temp['time_client']),
					'time_request'	=> $result_temp['time_request'],
					'time_schedule'	=> $result_temp['time_schedule'],
					'subject'	=> $result_temp['subject'],
					'sender_id' => $result_temp['sender_id']
				];
				if ($type == 'social') {
					$this->request['social_id']			= $value[0]['social_id'];
					$this->request['provider']			= $value[0]['provider'];
					$this->request['message']			= $value[0]['message'];
					$this->request['recipient']			= $value[0][$this->index_recipient[$type]];
					$this->request['channel'][$type]	= [$value[0]];
					$this->request['data'] 				= $value[0];
				}
				else {
					$this->request['provider']			= $value['provider'];
					$this->request['message']			= $value['message'];
					$this->request['recipient']			= $value[$this->index_recipient[$type]];
					$this->request['channel'][$type]	= $value;
					$this->request['data']				= $value;
				}
				$f_breaked = true;
				break;
			}
			if(strtolower($this->request['channel']) == strtolower($type)) {
				$this->request_prev['provider'] = $value['provider'];
				$f_break = true;
			}
		}
		return $f_breaked;
	}

	private function callback_backup_channel ($t_prefix) {
		$result_temp	= $this->select_from_temp($t_prefix);
		$channel_db		= json_decode($result_temp['param'], true);
		$this->request_prev	= $this->request;

		if (strtolower($this->request['channel']) == 'social') {
			if ($this->callback_backup_social($channel_db, $result_temp))
				$this->callback_send($t_prefix);
			else if ($this->callback_backup_type($channel_db, $result_temp))
				$this->callback_send($t_prefix);
			else {
				$this->request = null;
				$this->g_division = $this->get_url_client($this->recv[0]['div_id']);
				$this->write(__FUNCTION__, "no one next channel, end backup");
				$this->delete_by_id_from_temp($result_temp["id"], $t_prefix);
			}
		}
		else {
			if ($this->callback_backup_type($channel_db, $result_temp))
				$this->callback_send($t_prefix);
			else {
				$this->request = null;
				$this->g_division = $this->get_url_client($this->recv[0]['div_id']);
				$this->write(__FUNCTION__, "no one next channel, end backup");
				$this->delete_by_id_from_temp($result_temp["id"], $t_prefix);
			}
		}
	}

	private function callback_send ($t_prefix) {
		$res_recepient	= $this->recipient_check($this->request['ch_now'], false);
		$res_corporate	= $this->set_division(false);
		$res_sender		= $this->set_sender(false);
		if ($res_recepient && $res_corporate && $res_sender) {
			$this->insert_to_recv(
				$this->request['ch_now'],
				$this->request['recipient'],
				$this->request['provider'],
				$this->request['social_id']
			);
			$this->channel[$this->request['ch_now']]->sending(
				$this->request['ref_id']."_$t_prefix",
				$this->request['data'],
				$this->g_division,
				$this->g_group_send,
				$this->request
			);
		}
		else {
			$this->insert_to_recv(
				$this->request['ch_now'],
				$this->request['recipient'],
				$this->request['provider'],
				$this->request['social_id']
			);
			$this->write(__FUNCTION__, "Failed on recipient_check, set_division and set_sender");
			$this->channel[$index]->sendToCallback();
		}
	}

	private function callback_client_dlvr () {
		$exp_ref_id = explode('_', $this->request['ref_id']);
		$this->request['ref_id'] = $exp_ref_id[0];
		$recv = $this->check_recv($this->request['channel']);
		if (!$recv) {
			$this->write(__FUNCTION__, 'recv not found');
			$this->response->INTERNAL_ERROR();
		}

		$url_client = $this->get_url_client($recv[0]['div_id']);
		if (!isset($this->request['social_id'])) $this->request['social_id'] = "";
		$data_to_send = [
			"ref_id" 		=> $this->request['ref_id'],
			"status"		=> $this->request['status'],
			"channel"		=> $this->request['channel'],
			"social_id"		=> $this->request['social_id'],
			'provider'		=> $recv[0]['provider'],
			"recipient"		=> $recv[0]['recipient'],
			"time_delivery" => date("Y-m-d H:i:s", $this->request['time_dlvr'])
		];
		$thread = Sequence::getNextTrdSequence();
		$this->insert_toclient($url_client['url_delivery'], $data_to_send, $thread);
		$this->response->SUCCESS();
	}

	private function callback_client_sent ($prev, $next) {
		if (!isset($prev['social_id'])) $prev['social_id'] = '';
		$data_to_send = [
			'ref_id'	=> $prev['ref_id'],
			'previous'	=> [
				'status'			=> $prev['status'],
				'sprint_to_provider'=> date('Y-m-d H:i:s', $prev['time_send']),
				'provider_receive'	=> date('Y-m-d H:i:s', $prev['time_prov_recv']),
				'provider'			=> $prev['provider'],
				'channel'			=> $prev['channel'],
				'social_id'			=> $prev['social_id'],
				'recipient'			=> $this->recv[0]['recipient']
			]
		];
		if (!is_null($next)) {
			$data_to_send['next'] = [
				'channel'		=> $next['ch_now'],
				'social_id'		=> $next['social_id'],
				'recipient'		=> $next['recipient'],
				'provider'		=> $next['provider'],
				'subject'		=> $next['subject'],
				'provider'		=> $next['provider'],
				'body'			=> $next['message'],
				'time_request'	=> $next['time_request'],
				'time_schedule' => $next['time_schedule'],
				'reff_id'		=> $next['ref_id']
			];
		}
		$thread = Sequence::getNextTrdSequence();
		$this->insert_toclient($this->g_division['url_sent'], $data_to_send, $thread);
	}

}

?>
