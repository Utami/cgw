<?php

namespace App\Controller;
use \App\Controller\CgwFunction as CgwFunction;
use \App\Controller\Response as Response;
use \App\Utils\Sequence as Sequence;
use \App\Model\Databases as Databases;

class Callback extends CgwFunction {

	protected $request;
	protected $response;
	protected $channel;
	protected $g_division;
	protected $g_group_send;
	protected $databases;
	protected $app_log;

	public function __construct ($request, $APP_LOG = '') {
		parent::__construct($APP_LOG);
		$this->app_log		= $APP_LOG;
		$this->request		= $request;
		$this->response		= new Response($APP_LOG);
		$this->databases	= new Databases;
	}

	public function run () {
		$this->write(__FUNCTION__, "REQUEST=".json_encode($this->request));
		if (!$this->callback_check()) $this->response->INVALID_PARAM();
		$this->request['subject'] = $this->getdata_recv_h2h($this->request['ref_id'],$this->request['code_sms'],$this->request['channel']);
		$GLOBALS['code_sms']	= $this->request['code_sms'];
		$GLOBALS['ref_id']		= $this->request['ref_id'];
		if ($this->request['type_status'] == 'send')		$this->sent();
		else if ($this->request['type_status'] == 'dlvr')	$this->dlvr();
		else	$this->response->INVALID_PARAM();
		$this->response->SUCCESS();
	}

	private function dlvr () {
		$exp_ref_id = explode('_', $this->request['ref_id']);
		$this->request['ref_id'] = $exp_ref_id[0];
		$recv = $this->check_recv($this->request['channel']);
		if (!$recv) {
			$this->write(__FUNCTION__, 'recv not found');
			$this->response->INTERNAL_ERROR();
		}

		$url_client = $this->get_url_client($recv[0]['div_id']);
		if (!isset($this->request['social_id'])) $this->request['social_id'] = "";
		$data_to_send = [
			"ref_id" 		=> $this->request['ref_id'],
			"status"		=> $this->request['status'],
			"channel"		=> $this->request['channel'],
			"social_id"		=> $this->request['social_id'],
			'provider'		=> $recv[0]['provider'],
			"recipient"		=> $recv[0]['recipient'],
			"username"		=> $recv[0]['user'],
			"time_delivery" => date("Y-m-d H:i:s", $this->request['time_dlvr']),
			"time_request"	=> $recv[0]['time_request'],
		];
		$thread = Sequence::getNextTrdSequence();
		$this->insert_toclient($url_client['url_delivery'], $data_to_send, $thread);
		$this->insert_to_dlvr($thread);
	}

	private function sent () {
		$ref_id_exp = explode("_", $this->request["ref_id"]);
		if (count($ref_id_exp) > 1) {
			$this->write(__FUNCTION__, "processing callback type backup");
			$t_prefix = (int)$ref_id_exp[1];
			$this->request["ref_id"] = $ref_id_exp[0];
			if (!$this->insert_to_send_with_checking($this->request['channel'])) $this->response->DOUBLE_DATA();
			$get_flag = substr($this->request['subject'], 0, 3); //get value subject --> h2h
			if ($get_flag=='h2h') { //check if flaging is h2h
				$get_flag='h2h';
			} else {
				$get_flag='';
			}
			$temp = $this->select_from_temp($t_prefix,$get_flag);
			$this->request['time_request'] = $temp['time_request'];
			if ((int)$this->request["status"] != 1){
				if ($this->next_channel($t_prefix, $temp)) {
					$this->send_channel($t_prefix);
					$this->callback_client_sent($this->request_prev, $this->request);
				}
				else {
					//=== START TOKEN
					$ch_now = $this->request['channel'];
					$this->request['channel'] = json_decode($temp['param'], true);
					$this->request['username'] = $temp['user'];
					$this->set_division();
					$token_used = 0;
					foreach ($this->request['channel'] as $index => $data) {
						if (!$this->build_channel($index))
							$this->response->INTERNAL_ERROR();
						$c_token_used = $this->channel[$index]->token_used();
						if ($token_used < $c_token_used)
							$token_used = $c_token_used;
					}
					$this->token_reverse($token_used);
					//=== END TOKEN
					$this->g_division = $this->get_url_client($this->recv[0]['div_id']);
					$this->request['provider'] = $this->recv[0]['provider'];
					$this->write(__FUNCTION__, "no one next channel, end backup");
					$this->request['channel'] = $ch_now;
					$this->callback_client_sent($this->request, null);
					$this->delete_by_refnum_from_temp($this->request["ref_id"], $t_prefix, $get_flag);
				}
			}
			else {
				//=== START TOKEN
				$ch_now = $this->request['channel'];
				$this->request['channel'] = json_decode($temp['param'], true);
				$this->request['username'] = $temp['user'];
				$this->set_division();
				$token_used = 0;
				foreach ($this->request['channel'] as $index => $data) {
					if (!$this->build_channel($index))
						$this->response->INTERNAL_ERROR();
					$c_token_used = $this->channel[$index]->token_used();
					if ($token_used < $c_token_used)
						$token_used = $c_token_used;
				}
				$reverse = $token_used - $this->channel[$ch_now]->token_used();
				if ($reverse > 0) $this->token_reverse($reverse);
				//=== END TOKEN
				$this->g_division = $this->get_url_client($this->recv[0]['div_id']);
				$this->request['provider'] = $this->recv[0]['provider'];
				$this->write(__FUNCTION__, "status send 0 detected");
				$this->request['channel'] = $ch_now;
				$this->callback_client_sent($this->request, null);
				$this->delete_by_refnum_from_temp($this->request["ref_id"], $t_prefix, $get_flag);
			}
		}
		else {
			$this->write(__FUNCTION__, "processing callback type sendall");
			if (!$this->insert_to_send_with_checking($this->request['channel'])) $this->response->DOUBLE_DATA();
			$this->request['time_request'] = $this->recv[0]['time_request'];
			//=== START TOKEN
			if ($this->request['status'] != 1) {
				$ch_now = $this->request['channel'];
				$this->request['channel'] = [ $ch_now => []];
				$this->request['username'] = $this->recv[0]['user'];
				$this->set_division();
				// $this->write(__FUNCTION__, "channel ".$this->request['channel']." username : ".$this->request['username']." set_division : ".$this->set_division());
				if (!$this->build_channel($ch_now))
					$this->response->INTERNAL_ERROR();
				$c_token_used = $this->channel[$ch_now]->token_used();
				$this->token_reverse($c_token_used);
			}
			//=== END TOKEN

			//error_log(json_encode($this->recv));
			$this->request['provider'] = $this->recv[0]['provider'];
			$this->g_division = $this->get_url_client($this->recv[0]['div_id']);
			$this->callback_client_sent($this->request, null);
		}
	}

	private function next_channel ($t_prefix, $temp) {
		//$temp = $this->select_from_temp($t_prefix);
		$data_channel = json_decode($temp['param'], true);
		$this->request_prev = $this->request;
		$f_break = false;
		foreach ($data_channel as $type => $value) {
			if ($f_break) {
				$this->request = [
					'ch_now'	=> $type,
					'social_id'	=> '',
					'provider'	=> $value['provider'],
					'message'	=> $value['message'],
					'data'		=> $value,
					'ref_id'	=> $this->request['ref_id'],
					'username'	=> $temp['user'],
					'type'		=> 1,
					'time'		=> strtotime($temp['time_client']),
					'time_request'	=> $temp['time_request'],
					'time_schedule'	=> $temp['time_schedule'],
					'subject'	=> $temp['subject'],
					'sender_id' => $temp['sender_id'],
					'channel'	=> [
						$type => $value
					]
				];
				return true;
			}
			if(strtolower($this->request['channel']) == strtolower($type)) {
				$this->request_prev['provider'] = $value['provider'];
				$f_break = true;
			}
		}
		return false;
	}

	private function callback_client_sent ($prev, $next) {
		if (!isset($prev['social_id'])) $prev['social_id'] = '';
		$data_to_send = [
			'ref_id'	=> $prev['ref_id'],
			'username'	=> $this->recv[0]['user'],
			'time_request'=> $prev['time_request'],
			'previous'	=> [
				'status'			=> $prev['status'],
				'sprint_to_provider'=> date('Y-m-d H:i:s', $prev['time_send']),
				'provider_receive'	=> date('Y-m-d H:i:s', $prev['time_prov_recv']),
				'provider'			=> $prev['provider'],
				'channel'			=> $prev['channel'],
				'social_id'			=> $prev['social_id'],
				'recipient'			=> $this->recv[0]['recipient']
			]
		];
		if (!is_null($next)) {
			$data_to_send['next'] = [
				'channel'		=> $next['ch_now'],
				'social_id'		=> $next['social_id'],
				'recipient'		=> $next['recipient'],
				'provider'		=> $next['provider'],
				'subject'		=> $next['subject'],
				'provider'		=> $next['provider'],
				'body'			=> $next['message'],
				'time_request'	=> $next['time_request'],
				'time_schedule' => $next['time_schedule'],
				'reff_id'		=> $next['ref_id'],
			];
		}
		$thread = Sequence::getNextTrdSequence();
		$this->insert_toclient($this->g_division['url_sent'], $data_to_send, $thread);
	}

	private function send_channel ($t_prefix) {
		$flag = true;
		if (!$this->set_division())	{
			$this->write(__FUNCTION__, "INVALID_CORPORATE on next backup");
			$flag = false;
		} else $this->g_division['id-g-send'] = $this->request['sender_id'];
		if (!$this->set_sender()) {
			$this->write(__FUNCTION__, "UNKNOWN_SENDER_ID on next backup");
			$flag = false;
		}
		if (!$this->build_channel($this->request['ch_now'])) {
			$this->write(__FUNCTION__, "INTERNAL_ERROR on next backup");
			$flag = false;
		}
		if (!$this->channel[$this->request['ch_now']]->param_check()) {
			$this->write(__FUNCTION__, "INVALID_PARAM on next backup");
			$flag = false;
		}
		if (!$this->channel[$this->request['ch_now']]->recipient_check())	{
			$this->write(__FUNCTION__, "INVALID_RECIPIENT on next backup");
			$flag = false;
		}
		if ($flag) {
			$this->request['recipient'] = $this->channel[$this->request['ch_now']]->recipient;
			$this->insert_to_recv(
				$this->request['ch_now'],
				$this->request['recipient'],
				$this->request['provider']
			);
			$result = $this->channel[$this->request['ch_now']]->sending(
				$this->request['ref_id']."_$t_prefix",
				$this->g_division,
				$this->g_group_send,
				$this->request
			);
			if ((int)$result['rc'] != 0) {
				$this->channel[$this->request['ch_now']]->sendToCallback($result['rc']);
			}
		}
	}

}
