<?php

namespace App\Controller;
use \App\Utils\Loging as Loging;

class CgwFunction extends Loging {

	protected function callback_check () {
		$mandatory_param = [
			'ref_id',
			'type_status',
			'code_sms',
			'status',
			'channel'
		];
		foreach ($mandatory_param as $index) {
			if (!isset($this->request[$index])) {
				$this->write(__FUNCTION__, "parameter callback $index not complete mandatory");
				return false;
			}
		}

		if ($this->request['type_status'] == 'send') {
			if (!isset($this->request['time_send'])) {
				$this->write(__FUNCTION__, "parameter time_send $index not set");
				return false;
			}
			if (!isset($this->request['time_prov_recv'])) {
				$this->write(__FUNCTION__, "parameter time_prov_recv $index not set");
				return false;
			}
		}
		else if ($this->request['type_status'] == 'dlvr') {
			if (!isset($this->request['time_dlvr'])) {
				$this->write(__FUNCTION__, "parameter time_dlvr $index not set");
				return false;
			}
		}
		else {
			$this->write(__FUNCTION__, "value parameter type_status $index unknown");
			return false;
		}
		return true;
	}

	protected function request_check () {
		foreach ([
			'ref_id',
			'username',
			'type',
			'time',
			'time_request',
			'time_schedule',
			'signature',
			'subject',
			'sender_id',
			'channel'
		] as $index) {
			if (!isset($this->request[$index])) {
				$this->write(__FUNCTION__, "param $index not found");
				return false;
			}
			if ($index == 'ref_id') $GLOBALS['ref_id'] = $this->request['ref_id'];
		}
		
		if (
			!is_array($this->request["channel"]) ||
			$this->request["channel"] == null ||
			empty($this->request["channel"])
		) {
			$this->write(__FUNCTION__, "channel error");
			return false;
		}

		if (strpos($this->request["ref_id"], '_')) {
			$this->write(__FUNCTION__, "ref_id use character _");
			return false;
		}
		if (!in_array((int)$this->request['type'], [1,2])) {
			$this->write(__FUNCTION__, "type transaction not allowed");
			return false;
		}
		return true;
	}

	protected function build_channel ($index) {
		if (@include_once(__DIR__."/../../Libs/$index.php")) {
			if (class_exists($index)) {
				$this->channel[$index] = new $index(
					$this->request['channel'][$index],
					$this->databases,
					$this->app_log
				);
				return true;
			}
			else {
				$this->write(__FUNCTION__, "Error class $index not found");
				return false;
			}
		}
		else {
			$this->write(__FUNCTION__, "Error file $index on libs not found");
			return false;
		}
	}

	protected function check_sender_id () {
		$temp_senderid = json_decode($this->g_division['id-g-send'], true);
		if (in_array($this->request['sender_id'], $temp_senderid)) {
			//$this->write(__FUNCTION__, "renew id-g-send from g_division");
			$this->g_division['id-g-send'] = $this->request['sender_id'];
			return true;
		}
		else {
			$this->write(__FUNCTION__, "not found g_send, compare from request and sender id");
			return false;			
		}
	}

	protected function set_division () {
		/* START 
		$this->g_division = array (
			'id' => '15',
			'id-c' => '3',
			'id-g-send' => '["17","18"]',
			'id-g-routes' => '2',
			'name' => 'MySprint OTP',
			'password' => 'mysprintotp',
			'ip_whitelist' => '*',
			'token_type' => 'u',
			'token' => '0',
			'service_type' => 'otp',
			'url_sent' => '192.168.1.112:9002/api/Report/SendStatus',
			'url_delivery' => '192.168.1.112:9002/api/Report/StatusDelivery',
			'url_callback' => 'http://192.168.1.111:9001/json/callback',
		);
		return true;
		 END */
		$result = $this->databases->query(
			'g_division',
			"SELECT `id`, `id-c`, `id-g-send`, `id-g-routes`, ".
			"`name`, `password`, `ip_whitelist`, `token_type`, ".
			"`token`, `service_type`, `url_sent`, `url_delivery`, ".
			"`url_callback` FROM `g_division` ".
			"WHERE `username` = '{$this->request['username']}'"
		);
		$result = mysqli_fetch_all($result, MYSQLI_ASSOC);
		if (count($result) > 0) {
			$this->g_division = $result[0];
			//$this->write(__FUNCTION__, var_export($this->g_division, true));
			return true;
		}
		else {
			$this->write(__FUNCTION__, "tcp user not found");
			return false;
		}
	}

	protected function get_url_client ($id, $flag = true) {
		$result = $this->databases->query(
			'g_division',
			"SELECT `url_sent`, `url_delivery` ".
			" FROM `g_division` ".
			"WHERE `id` = $id"
		);
		$result = mysqli_fetch_all($result, MYSQLI_ASSOC);
		if (count($result) > 0) {
			//$this->write(__FUNCTION__, var_export($result[0], true));
			return $result[0];
		}
		else {
			$this->write(__FUNCTION__, "id user not found");
			if ($flag) $this->response->INVALID_CORPORATE();
			else return false;
		}
	}

	protected function sign_check () {
		$compose = $this->request["username"].$this->g_division["password"].$this->request["time"];
		$sign = hash('sha256', $compose);
		//$this->write(__FUNCTION__, "compare with $sign");
		if ($sign != $this->request["signature"]) return false;
		return true;
	}

	protected function ip_check () {
		$client_ip = $_SERVER['REMOTE_ADDR'];
		if ($this->g_division != null) {
			$whitelist = explode(",", $this->g_division['ip_whitelist']);
			if (is_array($whitelist) && $whitelist != null) {
				foreach ($whitelist as $ip_address) {
					if (trim($ip_address) === '*' || trim($ip_address) === trim($client_ip))
						return true;
				}
			}
			else {
				$this->write(__FUNCTION__, "ip_whitelist not array/null");
				return false;
			}
		}
		else {
			$this->write(__FUNCTION__, "g_division is null");
			return false;
		}
		$this->write(__FUNCTION__, "ip not in whitelist $client_ip");
		return false;
	}

	protected function token_used ($token) {
		/* START 
		return $this->databases->query(
			$GLOBALS['DB_TRX_CONFIG'],
			"INSERT INTO `360_token_flag` SET `id_corporate` = {$this->g_division['id']}"
		);
		return true;
		 END */
		return $this->databases->query(
			't_token',
			"UPDATE `t_token` SET `token` = `token` - ".$token." WHERE ".
			"`id-c` = {$this->g_division['id-c']} AND ".
			"`id-d` = {$this->g_division['id']} AND ".
			"`token` >= $token",
			true,
			true
		);
	}

	protected function token_reverse ($token) {
		/* START 
		return true;
		 END */
		return $this->databases->query(
			't_token',
			"UPDATE `t_token` SET `token` = `token` + $token WHERE ".
			"`id-c` = {$this->g_division['id-c']} AND ".
			"`id-d` = {$this->g_division['id']} AND ".
			"`token` >= $token",
			true
		);
	}

	protected function insert_to_temp ($t_prefix = "1") {
		/* START 
		return true;
		 END */
		return $this->databases->query(
			"360_tempdata_$t_prefix",
			"INSERT INTO `360_tempdata_$t_prefix` SET ".
			"`refnum` = '{$this->request['ref_id']}', ".
			"`code_sms` = '{$GLOBALS['code_sms']}', ".
			"`type` = '{$this->request['type']}', ".
			"`sender_id` = {$this->request['sender_id']}, ".
			"`subject` = '{$this->request['subject']}', ".
			"`user` = '{$this->request['username']}', ".
			"`sign` = '{$this->request['signature']}', ".
			"`param` = '".json_encode($this->request["channel"])."', ".
			"`time_request` = '{$this->request['time_request']}', ".
			"`time_schedule` = '{$this->request['time_schedule']}', ".
			"`time_client` = '".date("Y-m-d H:i:s", (int)$this->request["time"])."' ",
			true
		);
	}

	protected function select_from_temp ($t_prefix) {
		$result = $this->databases->query(
			"360_tempdata_$t_prefix",
			"SELECT `id`, `sender_id`, `subject`, `user`, `param`, `time_request`, `time_schedule`, `time_client` ".
			"FROM `360_tempdata_$t_prefix` WHERE `refnum` = '{$this->request['ref_id']}'"
		);
		$result = mysqli_fetch_all($result, MYSQLI_ASSOC);
		if (count($result) > 0) {
			return $result[0];
		}
		else {
			$this->write(__FUNCTION__, "not found data");
			$this->response->INTERNAL_ERROR();
		}
	}

	protected function delete_by_id_from_temp ($id, $t_prefix) {
		return $this->databases->query(
			"360_tempdata_$t_prefix",
			"DELETE FROM `360_tempdata_$t_prefix` WHERE `id` = $id"
		);
	}

	protected function delete_by_refnum_from_temp ($refnum, $t_prefix) {
		return $this->databases->query(
			"360_tempdata_$t_prefix",
			"DELETE FROM `360_tempdata_$t_prefix` WHERE `refnum` = '$refnum'",
			true
		);
	}

	protected function set_sender () {
		/* START 
		$this->g_group_send = array (
			'name' => 'Operator Test 1',
		);
		return true;
		 END */
		$result = $this->databases->query(
			'g_group_send',
			"SELECT `name` FROM `g_group_send` WHERE `id` = {$this->request['sender_id']}"
		);
		$result_gsender = mysqli_fetch_all($result, MYSQLI_ASSOC);
		if (count($result_gsender) > 0) {
			$this->g_group_send = $result_gsender[0];
			return true;
		}
		else {
			$this->write(__FUNCTION__, "sender_id unknown ".$this->request["sender_id"]);
			return false;
		}
	}

	protected function insert_to_recv ($channel, $recipient, $provider) {
		/* START 
		return true;
		 END */
		$query = "INSERT INTO `360_report_recv` SET ".
			"`corp_id` = {$this->g_division['id-c']}, ".
			"`div_id` = {$this->g_division['id']}, ".
			"`refnum` = '{$this->request['ref_id']}', ".
			"`type` = '{$this->request['type']}', ".
			"`code_sms` = '{$GLOBALS['code_sms']}', ".
			"`subject` = '{$this->request['subject']}', ".
			"`recipient` = '$recipient', ".
			"`user` = '{$this->request['username']}', ".
			"`channel` = '$channel', ".
			"`provider` = '$provider', ".
			"`time_request` = '{$this->request['time_request']}', ".
			"`time_schedule` = '{$this->request['time_schedule']}', ".
			"`time_client` = '".date("Y-m-d H:i:s", (int)$this->request["time"])."', ".
			"`time_recv` = '{$GLOBALS['time_recv']}'";
		return $this->databases->query('360_report_recv', $query, true);
	}

	protected function insert_to_send () {
		/* START 
		return true;
		 END */
		$query = "INSERT INTO `360_report_send` SET ".
			"`refnum` = '{$this->request['ref_id']}', ".
			"`code_sms` = '{$this->request['code_sms']}', ".
			"`channel` = '{$this->request['channel']}', ".
			"`status` = '{$this->request['status']}'";
		if (isset($this->request['time_send'])) $query .= " , `time_send` = '".date("Y-m-d H:i:s", (int)$this->request["time_send"])."'";
		if (isset($this->request['time_prov_recv'])) $query .= ", `time_prov_recv` = '".date("Y-m-d H:i:s", (int)$this->request["time_prov_recv"])."'";
		$this->databases->query('360_report_send', $query, true);
	}

	/*
	protected function insert_to_sendtoclient () {
		$this->write(__FUNCTION__, "processing for report client");
	}
	*/

	protected function insert_to_send_with_checking ($channel_now) {
		$this->recv = $this->check_recv($channel_now);
		if ($this->recv) {
			if ($this->check_send($channel_now)) {
				$this->write(__FUNCTION__, "failed save to send because avaiable on send");
				return false;
			}
			else {
				$this->insert_to_send();
				return true;
			}
		}
		else {
			$this->write(__FUNCTION__, "failed save to send because not avaiable on recv");
			return false;
		}
	}

	protected function check_send ($channel_now) {
		$query = "SELECT `id` FROM `360_report_send` WHERE `refnum` = '{$this->request['ref_id']}' ".
			"AND `code_sms` = '{$this->request['code_sms']}' AND `channel` = '$channel_now'";
		$result = $this->databases->query('360_report_send', $query);
		$result = mysqli_fetch_all($result, MYSQLI_ASSOC);	
		if (count($result) > 0) {
			return $result;
		}
		else {
			return false;
		}
	}

	protected function check_recv ($channel_now) {
		$query = "SELECT `user`, `div_id`, `subject`, `recipient`, `provider`, `time_request`, `time_schedule` ".
			"FROM `360_report_recv` WHERE `refnum` = '{$this->request['ref_id']}' ".
			"AND `code_sms` = '{$this->request['code_sms']}' AND `channel` = '$channel_now'";
		$result = $this->databases->query('360_report_recv', $query);
		$result = mysqli_fetch_all($result, MYSQLI_ASSOC);
		if (count($result) > 0) {
			return $result;
		}
		else {
			return false;
		}
	}

	protected function insert_toclient ($url, $data, $thread) {
		/* START 
		return true;
		 END */
		$query = "INSERT INTO `360_to_client` SET ".
			"`url` = '$url', ".
			"`data` = '".json_encode($data)."', ".
			"`thread` = $thread";
		$this->databases->query('360_to_client', $query, true);
	}

	protected function insert_to_dlvr ($thread) {
		/* START 
		return true;
		 END */
		$query = "INSERT INTO `360_report_dlvr` SET ".
			"`refnum` = '{$this->request['ref_id']}', ".
			"`code_sms` = '{$this->request['code_sms']}', ".
			"`channel` = '{$this->request['channel']}', ".
			"`status` = {$this->request['status']}, ".
			"`thread` = $thread, ".
			"`time_dlvr` = '".date("Y-m-d H:i:s", $this->request['time_dlvr'])."'";
		$this->databases->query('360_report_dlvr', $query, true);
	}

}

?>
