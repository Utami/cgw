<?php

namespace App\Controller;
use \App\Controller\CgwFunction as CgwFunction;
use \App\Controller\Response as Response;
use \App\Utils\Sequence as Sequence;
use \App\Utils\GetValue as GetValue;
use \App\Model\Databases as Databases;

class Request extends CgwFunction {

	protected $request;
	protected $response;
	protected $channel;
	protected $g_division;
	protected $g_group_send;
	protected $databases;
	protected $app_log;

	public function __construct ($request, $APP_LOG = '') {
		parent::__construct($APP_LOG);
		$this->app_log		= $APP_LOG;
		$this->request		= $request;
		$this->response		= new Response($APP_LOG);
		$this->databases	= new Databases;
	}

	public function run () {
		$this->write(__FUNCTION__, "REQUEST=".json_encode($this->request));
		if (!$this->request_check())	$this->response->INVALID_PARAM();
		if (!$this->set_division())		$this->response->INVALID_CORPORATE();
		if (!$this->check_sender_id())	$this->response->UNKNOWN_SENDER_ID();
		if (!$this->sign_check())		$this->response->INVALID_SIGN();
		// if (!$this->ip_check())			$this->response->NOT_IP_WHITELIST(); //sengaja di comen
		if (!$this->set_sender())		$this->response->UNKNOWN_SENDER_ID();
		//if (!$this->token_check())		$this->response->NOT_ENOUGHT_TOKEN();
		$GLOBALS['code_sms']	= GetValue::ReplyCode();
		$GLOBALS['ref_id']		= $this->request['ref_id'];
		if ((int)$this->request["type"] == 1)		$this->type_backup();
		else if ((int)$this->request["type"] == 2)	$this->type_sendall();
		$this->response->SUCCESS();
	}

	private function type_sendall() {
		$this->write(__FUNCTION__, "processing sendall");
		//=== START TOKEN
		$token_used = 0;
		foreach ($this->request['channel'] as $index => $data) {
			if (!$this->build_channel($index)) {
				$this->write(__FUNCTION__, "error build_channel $index");
				$this->response->INTERNAL_ERROR();
			}
			$token_used += $this->channel[$index]->token_used();
		}
		if ($this->g_division['token_type'] == 't') {
			if ($this->token_used($token_used) < 1)
				$this->response->NOT_ENOUGHT_TOKEN();
		}
		//=== END TOKEN
		foreach ($this->request["channel"] as $index => $data) {
			if (!$this->channel[$index]->param_check()) {
				$this->write(__FUNCTION__, "error param_check $index");
				$this->response->INVALID_PARAM();
			}
			if (!$this->channel[$index]->recipient_check())	{
				$this->write(__FUNCTION__, "error recipient_check $index");
				$this->response->INVALID_RECIPIENT();
			}

			if (isset($this->request['host'])) { //if field host for h2h
				$this->request['subject'] = 'h2h_'.$this->request['subject'];
			}
			$get_flag = substr($this->request['subject'], 0, 3); //check subject is h2h

			$this->insert_to_recv(
				$index,
				$this->channel[$index]->recipient,
				$data['provider'],
				$get_flag,
				$this->request['channel'][$index]['message']
			);
			$result = $this->channel[$index]->sending (
				$this->request["ref_id"],
				$this->g_division,
				$this->g_group_send,
				$this->request
			);
			if ((int)$result['rc'] == 99) {
				if (!isset($data['social_id'])) $data['social_id'] = "";
				$data_to_send = [
					'ref_id'	=> $this->request['ref_id'],
					'username'	=> $this->request['username'],
					'previous'	=> [
						'status'			=> $result['rc'],
						'sprint_to_provider'=> '',
						'provider_receive'	=> '',
						'provider'			=> $data['provider'],
						'channel'			=> $index,
						'social_id'			=> $data['social_id'],
						'recipient'			=> $this->channel[$index]->recipient
					]
				];
				$thread = Sequence::getNextTrdSequence();
				$this->insert_toclient($this->g_division['url_sent'], $data_to_send, $thread);
			}
		}
	}

	private function type_backup () {
		$this->write(__FUNCTION__, "processing backup");
		//=== START TOKEN
		$token_used = 0;
		foreach ($this->request['channel'] as $index => $data) {
			if (!$this->build_channel($index)) {
				$this->write(__FUNCTION__, "error build_channel $index");
				$this->response->INTERNAL_ERROR();
			}
			$c_token_used = $this->channel[$index]->token_used();
			if ($token_used < $c_token_used)
				$token_used = $c_token_used;
		}
		if ($this->g_division['token_type'] == 't') {
			if ($this->token_used($token_used) < 1)
				$this->response->NOT_ENOUGHT_TOKEN();
		}
		//=== END TOKEN
		$index = key($this->request['channel']);
		if (!$this->build_channel($index)) {
			$this->write(__FUNCTION__, "error build_channel $index");
			$this->response->INTERNAL_ERROR();
		}
		if (!$this->channel[$index]->param_check()) {
			$this->write(__FUNCTION__, "error param_check $index");
			$this->response->INVALID_PARAM();
		}
		if (!$this->channel[$index]->recipient_check())	{
			$this->write(__FUNCTION__, "error recipient_check $index");
			$this->response->INVALID_RECIPIENT();
		}
		$t_prefix = Sequence::getNextTrxSequence();

		if (isset($this->request['host'])) { //if field host for h2h
			$this->request['subject'] = 'h2h_'.$this->request['subject'];
		}
		$get_flag = substr($this->request['subject'], 0, 3); //check subject is h2h

		$this->insert_to_temp((string)$t_prefix,$get_flag); //flag == h2h
		$this->insert_to_recv(
			$index,
			$this->channel[$index]->recipient,
			$this->request['channel'][$index]['provider'],
			$get_flag,
			$this->request['channel'][$index]['message']
		); //flag == h2h
		$result = $this->channel[$index]->sending (
			"{$this->request['ref_id']}_$t_prefix",
			$this->g_division,
			$this->g_group_send,
			$this->request
		);
		// $this->write(__FUNCTION__, "resultdebuging : ".json_encode($result));
		if ((int)$result['rc'] == 99) {
			$this->channel[$index]->sendToCallback($result['rc']);
			if (!isset($this->request['channel'][$index]['social_id'])) $this->request['channel'][$index]['social_id'] = "";
			$data_to_send = [
				'ref_id'	=> $this->request['ref_id'],
				'username'	=> $this->request['username'],
				'previous'	=> [
					'status'			=> $result['rc'],
					'sprint_to_provider'=> '',
					'provider_receive'	=> '',
					'provider'			=> $this->request['channel'][$index]['provider'],
					'channel'			=> $index,
					'social_id'			=> $this->request['channel'][$index]['social_id'],
					'recipient'			=> $this->channel[$index]->recipient
				]
			];
			$thread = Sequence::getNextTrdSequence();
			$this->insert_toclient($this->g_division['url_sent'], $data_to_send, $thread);
		}
	}

}
