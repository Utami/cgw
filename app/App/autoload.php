<?php

require_once __DIR__ . "/../Config/ChannelIntegration.php";
require_once __DIR__ . "/Utils/Loging.php";
require_once __DIR__ . "/Controller/Response.php";
require_once __DIR__ . "/Utils/Sequence.php";
require_once __DIR__ . "/Controller/CgwFunction.php";
require_once __DIR__ . "/Model/Databases.php";
require_once __DIR__ . "/Model/Sender.php";
require_once __DIR__ . "/Utils/GetValue.php";
