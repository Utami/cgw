<?php

namespace App\Utils;
use \App\Utils\Loging;

class Sequence {

    private static $KEY_TRX_SEQUENCE 	= "__TRX_SEQUENCE__";
	private static $KEY_TRD_SEQUENCE 	= "__TRD_SEQUENCE__";

	public static function getNextTrxSequence() {
		$loging = new \App\Utils\Loging;
		$CT_TABLE = 1;
		if (isset($GLOBALS["APP_TEMPDATA_COUNT"])) {
			$CT_TABLE = (int)$GLOBALS["APP_TEMPDATA_COUNT"];
		} else $loging->write(__FUNCTION__, "CT_TABLE not configure");

        $value = apcu_fetch(self::$KEY_TRX_SEQUENCE);
		if ($value === false) {
			$value = 1;
		}

		$value++;
		if ($value > $CT_TABLE) {
			$value = 1;
		}
		apcu_store(self::$KEY_TRX_SEQUENCE, $value);
		return $value;
	}

	public static function getNextTrdSequence() {
		$loging = new \App\Utils\Loging;
		$CT_TABLE = 1;
		if (isset($GLOBALS["APP_THREAD_TO_CLIENT"])) {
			$CT_TABLE = (int)$GLOBALS["APP_THREAD_TO_CLIENT"];
		} else $loging->write(__FUNCTION__, "CT_TABLE not configure");

        $value = apcu_fetch(self::$KEY_TRD_SEQUENCE);
		if ($value === false) {
			$value = 1;
		}

		$value++;
		if ($value > $CT_TABLE) {
			$value = 1;
		}
		apcu_store(self::$KEY_TRD_SEQUENCE, $value);
		return $value;
	}

	public static function getNextThreadSequence($KEY, $CT_TABLE) {
        $value = apcu_fetch($KEY);
		if ($value === false) {
			$value = 1;
		}

		$value++;
		if ($value > $CT_TABLE) {
			$value = 1;
		}
		apcu_store($KEY, $value);
		return $value;
	}

	public static function resetTrxSequence() {
		//apcu_store(self::$KEY_TRX_SEQUENCE, 0);
		//apcu_store(self::$KEY_TRD_SEQUENCE, array());
        apcu_clear_cache(self::$KEY_TRX_SEQUENCE);
        apcu_clear_cache(self::$KEY_TRD_SEQUENCE);
		return 0;
	}

	public static function UUID() {
		//return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
		return sprintf( '%04x%04x%04x%04x%04x%04x%04x%04x',
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
			mt_rand( 0, 0xffff ),
			mt_rand( 0, 0x0fff ) | 0x4000,
			mt_rand( 0, 0x3fff ) | 0x8000,
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}

}

?>
