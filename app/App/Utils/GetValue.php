<?php

namespace App\Utils;

class GetValue {

	public static function GetMyMicrotime2() { //first_step
		$c = "1000000";
		while (strlen($c) > 6) {
		$b = explode(" ",microtime());
			$c = round($b['0']*1000000);
		}
		if (strlen($c) == "0") $c = "000000";
		elseif (strlen($c) == "1") $c = "00000"."$c";
		elseif (strlen($c) == "2") $c = "0000"."$c";
		elseif (strlen($c) == "3") $c = "000"."$c";
		elseif (strlen($c) == "4") $c = "00"."$c";
		elseif (strlen($c) == "5") $c = "0"."$c";
		return $c;
	}

	public function GetF7TrxID ($msisdn,$servid='',$mode='2') { //first_step
		$unix_id = "";
		$dateHIS = (date('H') * 3600) + (date('i') * 60) + date('s');
        $temp = "";
        for ($i=0; $i < (5 - strlen($dateHIS)); $i++) { $temp .=  "0"; }

		$dateDHIS =  (((date('d') - 1) * 1000 ) + 86400 ) + $dateHIS;
        $temp2 = "";
        for ($i=0; $i < (6 - strlen($dateDHIS)); $i++) { $temp2 .=  "0"; }

		$hp = substr($msisdn,-4);
        $hp2 = substr($msisdn,-6);
        if (strlen($servid) > 2) $servid = substr($servid,0,2);
        if (strlen($servid) == 1) $servid = "0".$servid;
		$lsid=strlen($servid);
		switch($lsid){
			case ($lsid > 2): $servid = substr($servid,0,2); break;
			case ($lsid == 1):  $servid = "0".$servid; break;
			case ($lsid == ""):  $servid = '99'; break;
		}
		switch($mode){
			case "1":
				$unix_id = $temp2.$dateDHIS.self::GetMyMicrotime2().self::RandNumber().self::RandNumber().self::RandNumber().self::RandNumber();
			break;
			case "2":
				$unix_id = $servid.$hp.$temp2.$dateDHIS.self::GetMyMicrotime3().self::RandNumber().self::RandNumber().self::RandNumber().self::RandNumber();
			break;
			case "3":
				$unix_id = $hp2.$temp.$dateHIS.self::GetMyMicrotime3().self::RandNumber();
			break;
			case "5";
				$unix_id = $servid.$hp.$temp2.self::$dateDHIS.self::RandNumber().self::RandNumber();
			break;
			case "7"://33 Digit
				$unix_id = date('Y').date('m').date('d').$servid.$hp2.$temp.$dateHIS.self::GetMyMicrotime2().self::RandNumber();
				$unix_id.=self::RandNumber().self::RandNumber().self::RandNumber().self::RandNumber().self::RandNumber();
			break;
			case "8":// 29 digit
				$unix_id = date('Y').date('m').date('d').$servid.$hp2.$temp.$dateHIS.self::GetMyMicrotime3();
				$unix_id.=self::RandNumber().self::RandNumber().self::RandNumber().self::RandNumber();
			break;
			default:
				$unix_id = $servid.$hp2.$temp.$dateHIS.self::GetMyMicrotime3().self::RandNumber().self::RandNumber().self::RandNumber();
		}
		return $unix_id;
	}// End GetF7TrxID

	public static function RandNumber() { //first_step
		$rv = array('1','2','3','4','5','6','7','8','9','0');
		srand((float)microtime()*1000000);
		return $rv[array_rand($rv,1)];
	}

	public static function Alpabetic() { //first_step
		$rv = array('1','2','3','4','5','6','7','8','9','0',
			'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',
			'p','q','r','s','t','u','v','w','x','y','z',
			'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
			'P','Q','R','S','T','U','V','W','X','Y','Z'
		);
		return $rv;
	}

	public static function RandAlpabet() { //first_step
		$rv = self::Alpabetic();
		srand((float)microtime()*1000000);
		return $rv[array_rand($rv,1)];
	}

	public static function ReplyCode () { //first_step
		$a = date('YmdHis');
		$c = self::GetMyMicrotime2();
		$b = self::RandNumber().self::RandNumber().self::RandNumber();
		$a = base64_encode("$a$c$b");
		$a = str_replace("=","",$a);
		$s = self::RandAlpabet();
		$r = self::RandAlpabet();
		$ret = trim("$s$a$r");
		return "$ret";
	}

	public function GetMyMicrotime3() { //first_step
		$c = "10000";
		while (strlen($c) > 4) {
			$b=explode(" ",microtime());
			$c=round($b['0']*10000);
		}
		switch(strlen($c)){
			case 0: $c="0000"; break;
			case 1: $c="000"."$c"; break;
			case 2: $c="00"."$c"; break;
			case 3: $c="0"."$c"; break;
		}
		return $c;
	}

}

?>
