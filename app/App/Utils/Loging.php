<?php

namespace App\Utils;

class Loging {
	
	private $fp;
    
	public function __construct ($filename = "") {
        global $APP_LOG;
        if ($filename == "") $filename = $APP_LOG;
		$this->fp = fopen($filename, "a+");
	}
	
    public function __destruct () {
        fclose($this->fp);
    }

    public function writeNoPrefix ($D) {
        fwrite($this->fp, $D.PHP_EOL);
    }

    public function write ($S, $D) {
        global $uniqid;
        $t      = explode(" ",microtime());
        $now    = date("Y-m-d H:i:s", $t[1]).substr((string)$t[0],1,4);
        $add    = $this->padding($S, 15);
        $prefix = "[$now][$uniqid][$add] ";
        fwrite($this->fp, $prefix.$D.PHP_EOL);
    }

    public function padding ($s, $len) {
        $result = $s;
        $len_s = strlen($s);
        if ($len_s > $len) {
            $result = substr($s, 0, $len);
        } 
        else if ($len_s < $len) {
            $pad_space = $len - $len_s;
            for ($i=0; $i<$pad_space; $i++) {
                $result .= " ";
            }
        }
        return $result;
    }


}
