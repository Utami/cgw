<?php
  function SenderToCgw($array_res = array())
    {
      global $URLSENDTOCGW,$PATHLOGH2H;
      $log = new \App\Utils\Loging($PATHLOGH2H);

      $data_string = json_encode($array_res);
      $log->write(__FUNCTION__,"REQUEST toCgw: url ->".$URLSENDTOCGW." ".$data_string."");

      $maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
      $ch = curl_init();
      $url = $URLSENDTOCGW;
      curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_string)));
      // curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
      $rs = curl_exec($ch);
      curl_close($ch);
      $log->write(__FUNCTION__,"RESPONSE fromCgw : ".$rs."\n");

      return $rs;
    }

    function ReportToSandeza($array_report = array())
      {
        global $URLREPORTTOSANDEZA,$PATHLOGH2H;
        $log = new \App\Utils\Loging($PATHLOGH2H);

        $data_string = json_encode($array_report);
        $log->write(__FUNCTION__,"REQUEST toSandeza : url ->".$URLREPORTTOSANDEZA." ".$data_string."");

        $maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
        $ch = curl_init();
        $url = $URLREPORTTOSANDEZA;
        curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        // curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
        $rs = curl_exec($ch);
        curl_close($ch);
        $log->write(__FUNCTION__,"RESPONSE fromSandeza : ".$rs."\n");

        return $rs;
      }

      function SendStsToSandeza($array_status = array())
        {
          global $URLSENSTSTOSANDEZA,$PATHLOGH2H;
          $log = new \App\Utils\Loging($PATHLOGH2H);

          $data_string = json_encode($array_status);
          $log->write(__FUNCTION__,"REQUEST Status toSandeza : url ->".$URLSENSTSTOSANDEZA." ".$data_string."");

          $maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
          $url = $URLSENSTSTOSANDEZA;
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
          // curl_setopt($ch, CURLOPT_HEADER, true);
          curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
          $rs = curl_exec($ch);
          curl_close($ch);
          $log->write(__FUNCTION__,"RESPONSE Status fromSandeza : ".$rs."\n");

          return $rs;
        }

      // function loginSystemSandeza(){
      //   global $PARAMAKUNSANDEZA,$URLLOGINSYSTEMSANDEZA;
      //
      //   $param = $PARAMAKUNSANDEZA;
      //
      //     $data_string = json_encode($param);
      //     $maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
      //     $ch = curl_init();
      //     $url = $URLLOGINSYSTEMSANDEZA;
      //     curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
      //     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      //     curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      //     // curl_setopt($ch, CURLOPT_HEADER, true);
      //     curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
      //     curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
      //     $rs = curl_exec($ch);
      //     curl_close($ch);
      //
      //     return $rs;
      // }
?>
