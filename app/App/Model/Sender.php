<?php

namespace App\Model;
use \App\Utils\Loging;

class Sender extends \App\Utils\Loging {

	public function go ($route_config, $data) { //first_step
		if (isset($route_config['method']) && strtoupper($route_config['method']) === "JSON") {
			return json_decode(self::json($route_config, $data), true);
		}
		else {
			$this->write(__FUNCTION__, "Sender go unknown method");
			return false;
		}
	}

	public function json ($route_config, $jsonData) { //first_step
		$url = $route_config['protocol']."://".$route_config['ip'].":".$route_config['port']."/".$route_config['path'];
		$jsonData = json_encode($jsonData);
		$ch = curl_init($url);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData);
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, (int)$route_config['timeout']);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	public static function chunkedresult($data) {
		$cont_type = "";
		$http_status_string = "";$http_status="";
		$temp = explode("\r\n\r\n",$data);
		$countPars = count($temp);
		$doChunk = "0";
		if ($countPars > 1) {
			$ret['header'] = trim($temp[0]);
			if ((preg_match('/<html>(.*)/i',trim($ret['header']),$match)) AND (preg_match('/<body>(.*)/i',trim($ret['header']),$match))){
				$ret['body'] = $data;
			}// end if preg_match
			else {
				$doChunk = "1";
				$ret['body']   = str_replace($ret['header'],"",$data);
				$statusTemp = "";
				if (preg_match('/^HTTP\/1\.[0-1] ([0-9][0-9][0-9] .*)\\r\\n/', $ret['header'], $match)) {
					$statusTemp = $match[1];
				}elseif (preg_match('/^HTTP\/1\.[0-1] ([0-9][0-9][0-9] .*)/', $ret['header'], $match)) {
					$statusTemp = $match[1];
				}// end if preg_match
			}// end if else preg_match
			$status = explode(" ",trim($statusTemp));
			if (isset($status[0])){ $http_status = $status[0]; }else{ $http_status = ""; }
			if (isset($match[0])){ $http_status_string = trim($match[0]); }else{ $http_status_string = ""; }
			if (preg_match('/Content\\-Type:\\s+([a-zA-Z\/]*)\\r\\n/',$ret['header'],$match)) { $cont_type = $match[1]; }elseif (preg_match('/Content\\-Type:\\s+([a-zA-Z\/]*)/',$ret['header'],$match)) { $cont_type = $match[1]; }// end if preg_match
			if ((strlen($cont_type) != (strlen(str_replace("xml","",$cont_type)))) AND ($cont_type != "")) { $cont_type = "xml"; }
		}// end if countPars
		if ($doChunk == "0") {
			$body = $data;
			if (substr(trim($data),0,5) == "<?xml") { $cont_type = "xml"; }
			if (preg_match('/^<html>(.*)<\/html>$/i',trim($data),$match)) {
				if (strlen(trim("<html>".$match[1]."</html>")) == strlen(trim($data)) ) {
					if (preg_match('/<body>(.*)<\/body>/i',trim($data),$match)){ $body = $match[1]; }
				}// end if strlen
			}// end if preg_match
			$ret['body'] = trim($body);
			$ret['header'] = "";
		}// end if doChunk
		$ret['http_status'] = $http_status;
		$ret['http_status_string'] = $http_status_string;
		if ($cont_type == ""){ $cont_type = "text/html"; }
		$ret['cont_type'] = $cont_type;

		return $ret;
	}// end chunkedresult

	public function getHtml ($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
		curl_setopt($ch, CURLOPT_TIMEOUT, "30"); // times out after $maxtime
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, "25"); // times out after $maxtryconencttime
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
		curl_setopt($ch, CURLOPT_HEADER, true);

		$rs = curl_exec($ch); // run the whole process
		$cont_type="";$response="";
		if(curl_errno($ch) == "0"){
			$ret['error']=0;
		  $temp = self::chunkedresult($rs);
		  if(!((preg_match('/2[0-9][0-9]/',$temp['http_status'],$match)) OR ($temp['http_status'] == ""))){
		  	$response=$temp['http_status_string'];
		  }else{
		  	$response=$temp['body'];
			$cont_type=$temp['cont_type'];
		  }
		}else{
		  $ret['error']=1;
		  $response="Error/Connection Failed";
		}
		$ret['full_iod']=$url;
		$ret['trx_date']=date('YmdHis');
		$ret['response']=trim($response);
		$ret['cont_type']=$cont_type;

		return $ret;
	}

}

?>
