<?php

namespace App\Model;
use mysqli, Exception;

class Databases {

	public function __contruct () {}

	public function loging ($query, $loging) {
		if ($loging) {
			$log = new \App\Utils\Loging($GLOBALS['DBS_LOG']);
			$log->writeNoPrefix($query);
			$log = null;
		}
	}

	public function query ($table_index, $query, $loging = false, $affect = false) {
		global $TABLE_MAPPING, $DATABASE;
		if (isset($DATABASE[$TABLE_MAPPING[$table_index]])) $config = $DATABASE[$TABLE_MAPPING[$table_index]];
		$conn = new mysqli(
			$config['server'],
			$config['username'],
			$config['password'],
			$config['database_name']
		);
		if (!$conn) {
			$this->loging($query, $loging);
			return false;
		} else {
			$result = $conn->query($query);
			if (!$result) $this->loging($query, $loging);
			if ($affect) return $conn->affected_rows;
			$conn->close();
			return $result;
		}
	}

}

?>
