<?php

namespace App\Model;
use mysqli, Exception;

class Databases {

	private $conn;

	public function __construct () {}

	private function conn ($c) {
		$index = $c['server'].$c['username'].$c['password'].$c['database_name'];
		if (isset($this->conn[$index])) {
			return $this->conn[$index];
		}
		else {
			$this->conn[$index] = new mysqli(
				$c['server'], 
				$c['username'], 
				$c['password'], 
				$c['database_name']
			);
			return $this->conn[$index];
		}
	}

	private function reconn ($c) {
		$index = $c['server'].$c['username'].$c['password'].$c['database_name'];
		$this->conn[$index] = new mysqli(
			$c['server'], 
			$c['username'], 
			$c['password'], 
			$c['database_name']
		);
	}

	public function query ($config, $query, $loging = false) {
		$conn = $this->conn($config);
		if ($conn->ping()) {
			try {
				$result = $conn->query($query);
				if ($loging && !$result) {
					$log = new \App\Utils\Loging($GLOBALS['DBS_LOG']);
					$log->writeNoPrefix($query);
					$log = null;
				}
				return $result;
			} catch (Exception $e) {
				$log = new \App\Utils\Loging($GLOBALS['DBS_LOG']);
				$log->writeNoPrefix($query);
				if ($loging) {
					$log = new \App\Utils\Loging($GLOBALS['DBS_LOG']);
					$log->writeNoPrefix($query);
					$log = null;
				}
				$conn->close();
				$this->reconn($config);
				return false;
			}
		}
		else {
			$log = new \App\Utils\Loging($GLOBALS['DBS_LOG']);
			$log->writeNoPrefix($query);
			if ($loging) {
				$log = new \App\Utils\Loging($GLOBALS['DBS_LOG']);
				$log->writeNoPrefix($query);
				$log = null;
			}
			$conn->close();
			$this->reconn($config);
			return false;
		}
	}

	public function __destruct () {
		foreach ($this->conn as $c) $c->close();
	}

}

?>