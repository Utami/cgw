<?php

namespace App\Model;
use mysqli, Exception;

class Databases {

	private $conn;
	private $app_log;

	public function __construct () {
		global $DATABASE, $APP_LOG;
		$this->app_log = $APP_LOG;
		foreach ($DATABASE as $index => $config)
			$this->connect($index, $config);
	}

	private function connect ($index, $config) {
		$log = new \App\Utils\Loging($this->app_log);
		for ($i=0; $i<3; $i++) {
			$this->conn[$index] = null;
			$this->conn[$index] = new mysqli (
				"p:".$config['server'], 
				$config['username'], 
				$config['password'], 
				$config['database_name']
			);
			if ($this->conn[$index]) break;
			$log->write("DB_ERROR", "RETRY ".($i+1)." CONNECTION ".$config['server']);
		}
		$log = null;
		return $this->conn[$index];
	}

	public function __destruct () {
		foreach ($this->conn as $conn) {
			if ($conn) $conn->close();
		}
	}
	
	public function loging ($query, $loging) {
		if ($loging) {
			$log = new \App\Utils\Loging($GLOBALS['DBS_LOG']);
			$log->writeNoPrefix($query);
			$log = null;
		}
	}

	public function query ($table_index, $query, $loging = false, $affect = false) {
		global $TABLE_MAPPING, $DATABASE;
		//if (isset($DATABASE[$TABLE_MAPPING[$table_index]])) $config = $DATABASE[$TABLE_MAPPING[$table_index]];
		$conn = $this->conn[$TABLE_MAPPING[$table_index]];
		if (!$conn) {
			$this->loging($query, $loging);
			$this->conn[$TABLE_MAPPING[$table_index]] = $this->connect($TABLE_MAPPING[$table_index], $DATABASE[$TABLE_MAPPING[$table_index]]);
			$conn = $this->conn[$TABLE_MAPPING[$table_index]];
			if ($conn) {
				$result = $conn->query($query);
				if ($result) return $result;
			}
			if (!$result) $this->loging($query, $loging);
			return false;
		} else {
			$result = $conn->query($query);
			if (!$result) $this->loging($query, $loging);
			if ($affect) return $conn->affected_rows;
			//$conn->close();
			return $result;
		}
	}

}

?>