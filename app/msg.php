<?php
$uniqid     = uniqid();

require_once __DIR__ . '/Config/ChannelIntegration.php';
require_once __DIR__ . '/Config/Connection_h2h.php';
require_once __DIR__ . '/App/Controller/H2hFunction.php';
require_once __DIR__ . '/App/Model/SenderH2h.php';
require_once __DIR__ . '/App/Utils/Loging.php';

header('Content-Type: application/json');

if (isset($_GET["u"]) && isset($_GET["p"]) && isset($_GET["d"]) && isset($_GET["m"])) {

      $array_req = array(
        'user' => $_GET["u"],
        'password' => $_GET["p"],
        'destination' => $_GET["d"],
        'message' => $_GET["m"]
      );

      CheckParam($array_req, true); //check parameter

      $dataDB = getDataFromDB($array_req); //get data from DB

      $arr_replace = array('[',']','"');
      $id_g_send = str_replace($arr_replace, '', $dataDB['id-g-send']);

      $username = $_GET["u"];
      $ref_id = GUID();
      $timestamp = DateTime::createFromFormat("YmdHis", date("YmdHis"));
      $time  = $timestamp->getTimestamp();
      $signature = hash( 'sha256', $dataDB['username'].''.$array_req['password'].''.$time );
      $subject = $time.'_'.$dataDB['username'];
      $sender_id = (int)$id_g_send;
      $time_request = date("Y-m-d H:i:s");
      $time_schedule = date("Y-m-d H:i:s");
      $provider = $dataDB['name'];

      $array_res = array(
        'type' => '2',
        'host' => 'h2h',
        'username' => $username,
        'ref_id' => strtolower($ref_id),
        'time' => strval($time),
        'signature' => $signature,
        'subject' => $subject,
        'sender_id' => $sender_id,
        'time_request' => $time_request,
        'time_schedule' => $time_schedule,
        'created_by' => 'Host',
        'channel' => array(
                      'sms' => array(
                        'msisdn' => $array_req['destination'],
                        'message' => $array_req['message'],
                        'provider' => $provider,
                        'order' => 99
                      ),
                    ),
      );
      // print_r(json_encode($array_res));
      // exit();
      $rs = SenderToCgw($array_res);

      print_r($rs);
      exit();
  } //end of check parameter
  else {
    
    $array_req = array(
      'user' => $_GET["u"],
      'password' => $_GET["p"],
      'destination' => $_GET["d"],
      'message' => $_GET["m"]
    );

    CheckParam($array_req,false);
    }
?>
