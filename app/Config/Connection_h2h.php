<?php

global $DATABASE;

$servername = $DATABASE['CFG']['server'];
$username = $DATABASE['CFG']['username'];
$password = $DATABASE['CFG']['password'];
$dbname = $DATABASE['CFG']['database_name'];

$servernametrx = $DATABASE['TRX']['server'];
$usernametrx = $DATABASE['TRX']['username'];
$passwordtrx = $DATABASE['TRX']['password'];
$dbnametrx = $DATABASE['TRX']['database_name'];
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$conntrx = new mysqli($servernametrx, $usernametrx, $passwordtrx, $dbnametrx);
// print_r($conntrx);
// exit();
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($conntrx->connect_error) {
    die("Connection failed: " . $conntrx->connect_error);
}

?>
