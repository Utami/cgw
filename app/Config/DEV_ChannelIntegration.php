<?php

$TABLE_MAPPING = [
	'360_report_dlvr'	=> 'TRX',
	'360_report_recv'	=> 'TRX',
	'360_report_send'	=> 'TRX',
	'360_tempdata_1'	=> 'TRX',
	'360_tempdata_2'	=> 'TRX',
	'360_tempdata_3'	=> 'TRX',
	'360_tempdata_4'	=> 'TRX',
	'360_tempdata_5'	=> 'TRX',
	'360_to_client'		=> 'TRX',
	'360_to_callback'	=> 'TRX',
	'g_corporate'		=> 'CFG',
	'g_division'		=> 'CFG',
	'g_group_send'		=> 'CFG',
	'g_provider'		=> 'CFG',
	'g_routes'			=> 'CFG',
	'g_send'			=> 'CFG',
	'p_prefix'			=> 'CFG',
	'p_send_config'		=> 'CFG',
	'routes'			=> 'CFG',
	't_token'			=> 'CFG',
	'db_prefix'			=> 'CFG'
];

$DATABASE = [
	'CFG'	=> [
		'database_name' => 'db_360',
		'server'        => '172.27.0.86',
		'username'      => 'appsysadmin',
		'password'      => 'passAPPSYSADMIN!'
	],
	'TRX'	=> [
		'database_name' => 'db_360_api',
		'server'        => '172.27.0.86',
		'username'      => 'appsysadmin',
		'password'      => 'passAPPSYSADMIN!'
	]
];

$APP_LOG = __DIR__."/../Logs_DEV/request-".date("Y-m-d").".log";
$DBS_LOG = __DIR__."/../Logs_DEV/error_query.log";

$APP_TEMPDATA_COUNT = 5;
$APP_THREAD_TO_CLIENT = 10;
$APP_THREAD_TO_CALLBACK = 10;

?>