<?php

/*
$DB_CLIENT_CONFIG = [
	'database_name' => 'db_360',
	'server'        => '192.168.1.118',
	'username'      => 'chandra',
	'password'      => 'cH4ndRA123&'
];

$DB_TRX_CONFIG = [
	'database_name' => 'db_360_api',
	'server'        => '192.168.1.118',
	'username'      => 'chandra',
	'password'      => 'cH4ndRA123&'
];
*/

$TABLE_MAPPING = [
	'360_report_dlvr'	=> 'TRX',
	'360_report_recv'	=> 'TRX',
	'360_reporth2h_recv'	=> 'TRX',
	'360_report_send'	=> 'TRX',
	'360_tempdata_1'	=> 'TRX',
	'360_tempdata_2'	=> 'TRX',
	'360_tempdata_3'	=> 'TRX',
	'360_tempdata_4'	=> 'TRX',
	'360_tempdata_5'	=> 'TRX',
	'360_tempdatah2h_1'	=> 'TRX',
	'360_tempdatah2h_2'	=> 'TRX',
	'360_tempdatah2h_3'	=> 'TRX',
	'360_tempdatah2h_4'	=> 'TRX',
	'360_tempdatah2h_5'	=> 'TRX',
	'360_to_client'		=> 'TRX',
	'360_to_callback'	=> 'TRX',
	'g_corporate'		=> 'CFG',
	'g_division'		=> 'CFG',
	'g_group_send'		=> 'CFG',
	'g_provider'		=> 'CFG',
	'g_routes'			=> 'CFG',
	'g_send'			=> 'CFG',
	'p_prefix'			=> 'CFG',
	'p_send_config'		=> 'CFG',
	'routes'			=> 'CFG',
	't_token'			=> 'CFG',
        't_token_stripo'                => 'CFG',
	'db_prefix'			=> 'CFG'
];

$DATABASE = [
	'CFG'	=> [
		'database_name' => 'db_360',
		'server'        => '172.27.0.86',
		'username'      => 'appsysadmin',
		'password'      => 'passAPPSYSADMIN!'
	],
	'TRX'	=> [
		'database_name' => 'db_360_api',
		'server'        => '172.27.0.86',
		'username'      => 'appsysadmin',
		'password'      => 'passAPPSYSADMIN!'
	]
];

$PARAMAKUNSANDEZA = array(
	 'username' => 'Host',
	 'password' => 'Host123!'
);

$APP_LOG 		= "/var/log/apps/360_cgw/request-".date("Y-m").".log";
$CLB_LOG		= "/var/log/apps/360_cgw/callback-".date("Y-m").".log";
$STCALLBACK_LOG	= "/var/log/apps/360_cgw/sendtocallback-".date("Y-m").".log";
$STCLIENT_LOG	= "/var/log/apps/360_cgw/sendtoclient-".date("Y-m").".log";
$DBS_LOG		= "/var/log/apps/360_cgw/error_query-".date("Y-m").".log";
$PATHLOGH2H = "/var/log/apps/360_cgw/sendh2h-".date("Y-m").".log";
$PATHLOGMSG = "/var/log/apps/360_cgw/sendmsg-".date("Y-m").".log";

$APP_TEMPDATA_COUNT = 5;
$APP_THREAD_TO_CLIENT = 10;
$APP_THREAD_TO_CALLBACK = 10;
$URLGETHTMLDEV = "http://172.27.0.104:81/api/Email/getHtml/";
$URLGETHTMLPROD = "http://172.27.0.104:81/api/Email/getHtml/";
$URLLOGINSYSTEMSANDEZA = "http://172.27.0.104:81/api/application/login";
$URLREPORTTOSANDEZA = "http://172.27.0.104:81/api/InsertReport";
$URLSENDTOCGW = "http://172.27.0.86:9001?opt=json/cgw";
$URLSENSTSTOSANDEZA = "http://172.27.0.104:8081/api/Report/SendStatus";
?>
