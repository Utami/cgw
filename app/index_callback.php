<?php

//$start = microtime(true);
$uniqid     = uniqid();
$time_recv  = date("Y-m-d H:i:s");

      if (isset($_GET["opt"])) {
       if ($_GET["opt"] == "json/callback" && !is_null($_POST)) {
              require_once __DIR__ . '/App/autoload.php';
              require_once __DIR__ . '/App/Controller/Callback.php';
              //$APP_LOG = __DIR__."/Logs/callback-".date("Y-m-d").".log";
      		$APP_LOG = $CLB_LOG;
              $json_request = json_decode(file_get_contents('php://input'), true);
              $cgw = new \App\Controller\Callback($json_request);
              $cgw->run();
              exit();
          }
      	/*
          else if ($_GET['opt'] == 'dev/cgw') {
              require_once __DIR__ . "/App/DEV_autoload.php";
              require_once __DIR__ . '/App/Controller/DEV_Request.php';
              $json_request = json_decode(file_get_contents('php://input'), true);
              $cgw = new \App\Controller\Request($json_request);
              $cgw->run();
              exit();
          }
          else if ($_GET['opt'] == 'dev/callback') {
              require_once __DIR__ . "/App/DEV_autoload.php";
              require_once __DIR__ . '/App/Controller/DEV_Callback.php';
              $json_request = json_decode(file_get_contents('php://input'), true);
              $cgw = new \App\Controller\Callback($json_request, $APP_LOG_DEV);
              $cgw->run();
              exit();
          }
      	*/
          else {
              require_once __DIR__ . "/App/autoload.php";
              $response = new \App\Controller\Response;
              $response->INTERNAL_ERROR();
              exit();
          }
      }
      else {
          require_once __DIR__ . "/App/autoload.php";
          $response = new \App\Controller\Response;
          $response->INTERNAL_ERROR();
      }


?>
