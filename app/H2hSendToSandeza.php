<?php
require_once __DIR__ . '/Config/ChannelIntegration.php';
require_once __DIR__ . '/Config/Connection_h2h.php';
require_once __DIR__ . '/App/Controller/H2hFunction.php';
require_once __DIR__ . '/App/Model/SenderH2h.php';

      $get_token = loginSystemSandeza();
      $token = json_decode($get_token, true);

      $db_check = CheckDataFromTbl(); //check parameter

      $dataDB=array();
      $id_data=array();
      $dat=array();
      foreach ($db_check['data'] as $key => $value) {
        $id_data[$key] = $value['id'];

        $dat[$key] = array(
          'user' => $value['user'],
          'div_id' => $value['div_id'],
          'recipient' => $value['recipient'],
          'channel' => $value['channel'],
          'message' => (!$value['message'])?"":$value['message'],
          'ref_id' => $value['refnum'],
          'code_sms' => $value['code_sms'],
          'time_request' => $value['time_request'],
          'time_schedule' => $value['time_schedule']
        );

        $dataDB[$key] = getDataFromDB2($dat[$key]);

        $arr_replace = array('[',']','"');
        $id_g_send = str_replace($arr_replace, '', $dataDB[$key]['id-g-send']);

        $username = $dataDB[$key]['username'];
        $ref_id = $dataDB[$key]['ref_id'];

        $arrtimereplace = array('-',' ',':');
        $time_replace = str_replace($arrtimereplace, '', $dataDB[$key]['time_request']);
        $time  = $time_replace;

        $signature = hash( 'sha256', $dataDB[$key]['username'].''.$dataDB[$key]['password'].''.$time );
        $subject = 'h2h_'.$time.'_'.$dataDB[$key]['username'];
        $sender_id = (int)$id_g_send;
        $time_request = $dataDB[$key]['time_request'];
        $time_schedule = $dataDB[$key]['time_schedule'];
        $provider = $dataDB[$key]['provider'];

        $leng_msg = strlen($dataDB[$key]['message'])/160;
        $MessageCount = ceil($leng_msg);

        $array_report[$key] = array(
          'MessageId' => $ref_id,
          'CorporateId' => '0a787303-583a-45f5-bfa1-044df509fd36',
          'DivisionId' => 'f57310dc-b47a-427b-9810-31fbf92720b3',
          'CreatedBy' => 'Kael',
          'Recipient' => $dataDB[$key]['recipient'],
          'Provider' => $provider,
          'Channel' => $dat[$key]['channel'],
          'Subject' => $subject,
          'Content' => $dataDB[$key]['message'],
          'RequestTime' => $time_request,
          'ScheduleTime' => $time_schedule,
          'SendToSprint' => $time_schedule,
          'ReceivedBySprintStatus' => '0',
          'ScheduleDefault' => $time_schedule,
          'MessageCount' => $MessageCount,
          'CodeSMS' => $dataDB[$key]['code_sms'],
          'Reschedule' => 0,
          'Reason' => 0,
          'senderId' => $sender_id,
          'order' => 1,
      	  'tipe' =>'file'
        );
        $rpt = ReportToSandeza($array_report[$key],$token['data']['token']);
        // $get_status_rpt = json_decode($rpt, true);
      }

      if (count($id_data)>0) {
        deleteDataH2h($id_data);
      }
      $conntrx->close();
      $conn->close();
      print_r('report berhasil dikirim...');
      exit();

?>
