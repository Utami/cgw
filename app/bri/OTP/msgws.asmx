<?php
//error_reporting(NULL);
//include "/var/www/bin/app/config.inc.php";
//include("config.inc.php");
//include("config_strip.php");
//include "/var/www/bin/app/func.inc.php";
//include("func.inc.php");
//include("func.baru.php");

#runConnectBRI231($dbhost231);

require_once('lib/nusoap.php');
$server = new soap_server();
$server->configureWSDL("WebServiceSMS", "urn:WebServiceSMS");

// ==== WSDL TYPES DECLARATION/RESPONSE ==============================================
$server->wsdl->addComplexType(
    "ClientResponse",
    "complexType",
    "struct",
    "all",
    "",
    array(
        "Status"        => array("name" => "Status", "type" => "xsd:int"),
        "Reason"        => array("name" => "Reason", "type" => "xsd:string"),
        "TimeRequest"   => array("name" => "TimeRequest", "type" => "xsd:string"),
        "TimeReply"     => array("name" => "TimeReply", "type" => "xsd:string")
    )
);

// ==== WSDL METHODS REGISTRATION ===========================================
$server->register(
    "WSSendSMS",
    array(
        "JenisSMS"      => "xsd:string",
        "KodeFitur"     => "xsd:string",
        "TimeSend"      => "xsd:string",
        "FlagDebet"     => "xsd:string",
        "FlagReply"     => "xsd:string",
        "ReffNo"        => "xsd:string",
        "MSISDN"        => "xsd:string",
        "SMSBody"       => "xsd:string"
    ),
    array("return" => "tns:ClientResponse"),
    "urn:WebServiceSMS",
    "urn:WebServiceSMS#WSSendSMS",
    "rpc",
    "encoded",
    "BRI Soap WerbServices"
);

$server->service(file_get_contents("php://input")); //FIXED 

function WSSendSMS($JenisSMS = '', $KodeFitur = '', $TimeSend = '', $FlagDebet = '', $FlagReply = '', $ReffNo = '', $msisdn = '', $sms = '')
{

    /*
    if ($FlagDebet == "1") {
        $username = 'SprBR1UoT8P9';
        $passwd = 'bR1567pwDtAb';
    } else {
        $username = 'BRIDRC2';
        $passwd = 'bR1dRc2';
    }
    */

    $dest = $msisdn;
    $sms = $sms;

    if (isset($dest['0'])) {
        if ($dest['0'] == "0") $dest = "62" . substr($dest, 1, strlen($dest));
    }
    $dest = str_replace("+", "", $dest);
    $dest = str_replace(" ", "", $dest);

    $deny = FALSE;

    //validation for BRI web service
    if (!is_numeric($JenisSMS) || $JenisSMS == '') {
        $code =  "1";
        $reason = "Format JenisSMS Salah.";
        $deny = true;
    }
    if ($JenisSMS < 0) {
        $code =  "1";
        $reason = "Format JenisSMS Salah.";
        $deny = true;
    } else {
        if ($JenisSMS <> 3) {
            $code =  "1";
            $reason = "Jenis SMS Salah.";
            $deny = true;
        }
    }


    if ($KodeFitur == '') {
        $code =  "1";
        $reason = "Gagal terkoneksi ke gateway.";
        $deny = true;
    }
    if ($sms == '') {
        $code =  "1";
        $reason = "Gagal insert ke gateway.";
        $deny = true;
    }
    if ($FlagDebet > 1 || !is_numeric($FlagDebet) || $FlagDebet == '') {
        $code =  "1";
        $reason = "Format FlagDebet Salah.";
        $deny = true;
    }
    if ($FlagReply > 1 || !is_numeric($FlagReply) || $FlagReply == '') {
        $code =  "1";
        $reason = "Format FlagReply Salah.";
        $deny = true;
    }

    $DivCodePref1 = substr($ReffNo, 0, 3);

    if (strlen($ReffNo) == 17) { //if div code 3 digit alpabet or 17 char
        if (!ctype_alpha($DivCodePref1)) {
            $code =  "1";
            $reason =  "DivCode Salah atau kosong.";
            $deny = true;
        }
    }
    if (strlen($ReffNo) < 17 || $ReffNo == '') {
        if (!ctype_alpha($DivCodePref1)) {
            $code =  "1";
            $reason =  "DivCode Salah atau kosong.";
            $deny = true;
        } else {
            $code =  "1";
            $reason =   "ReffNumber Redundant.";
            $deny = true;
        }
    }

    $DivCodePref2 = substr($ReffNo, 0, 4); //if div code 4 digit alpabet or 18 char
    if (strlen($ReffNo) == 18) {
        if (!ctype_alpha($DivCodePref2)) {
            $code =  "1";
            $reason =  "DivCode Salah atau kosong.";
            $deny = true;
        }
    }

    $result = preg_replace("/[^a-zA-Z]+/", "", $ReffNo);
    $totAlph =  strlen($result);
    if ($totAlph > 4) { //if total character in string more than 4
        $code =  "1";
        $reason =  "DivCode Salah atau kosong.";
        $deny = true;
    }

    if (strlen($ReffNo) > 18) {
        $code =  "1";
        $reason =  "ReffNumber Redundant.";
        $deny = true;
    }

    if (!(CheckFromNumber($dest)) || $dest == '') {
        //MSISDN is not In WhiteList.";
        $code =  "1";
        $reason =  "MSISDN Salah.";
        $deny = true;
    }

    $validityTime = 43200;    // 12 Jam dalam detik
    $now = new DateTime('NOW');
    $future = new DateTime($TimeSend);
    $diffSeconds = $now->getTimestamp() - $future->getTimestamp();

    if ($diffSeconds > $validityTime || $diffSeconds < ($validityTime * -1)) {
        $code =  "1";
        $reason =  "Di luar waktu validitas SMS.";
        $deny = true;
    }

    if (DateTime::createFromFormat('Y-m-d H:i:s', $TimeSend) == FALSE || $TimeSend == '') {
        $code =  "1";
        $reason =  "Format TimeSend Salah.";
        $deny = true;
    }

    //$cekTelco = CheckIsTruePrefixNumberTelco($dest);
    $cekTelco = "xl";
    if ($cekTelco == "") {
        $code =  "1";
        $reason = "Telco tidak mendukung.";
        $deny = true;
    }

    if (!($deny)) {
        if ($FlagDebet == "1") {
            $username = 'SprBR1UoT8P9';
            $passwd = 'bR1567pwDtAb';
        } else {
            $username = 'BRIDRC2';
            $passwd = 'bR1dRc2';
        }
        $dest = $msisdn;
        $sms = $sms;


        $TIMEOUT  = 30;
/*
        $URL = "http://127.0.0.1:9007/msgBri.php";
        $QS  = 'u=' . $username . '&p=' . $passwd . '&d=' . $dest . '&r=' . $ReffNo . '&m=' . $sms . '&t=' . $cekTelco . '  ';
        $URL .= "?" . $QS;

	
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, $TIMEOUT);

        $response = curl_exec($ch);
        curl_close($ch);

        $respCode = substr($response, 0, 1);
  */      
	$respCode = 0;

        if($respCode == 0){
            $result = array(
                "Status" => $respCode,
                "Reason" => "Success",
                "TimeRequest" => date("Y-m-d H:i:s"),
                "TimeReply" => date("Y-m-d H:i:s")
            );
            return $result;
        } else {
            $result = array(
                //"Status" => "2.".$respCode,
		"Status" => "1",
                //"Reason" => "msg.php error reply!",
		"Reason" => "Gagal insert ke gateway",
                "TimeRequest" => date("Y-m-d H:i:s"),
                "TimeReply" => date("Y-m-d H:i:s")
            );
            return $result;
        }

    } else {
        $result = array(
            "Status" => $code,
            "Reason" => $reason,
            "TimeRequest" => date("Y-m-d H:i:s"),
            "TimeReply" => date("Y-m-d H:i:s")
        );
        return $result;
        // return  $code . "-" . $msgID;
    }

    //runClose();
   // exit;
}


function CheckIsTruePrefixNumberTelco($msisdn)
{
    for ($i = 0; $i < 3; $i++) {
        $msisdnTemp = substr($msisdn, 0, $i + 5);

        $query = "SELECT * FROM view_prefix_sname ";
        $query .= "WHERE `number` = '" . addslashes($msisdnTemp) . "'";
        $res = runsql($query, "mobile_services");
        if ($res) {
            if ($row = mysql_fetch_array($res)) {
                if ($row[0] > 0) {
                    return $row['name']; //true;
                }
            }
        }
    }
    #return $insexec['name'];
}
