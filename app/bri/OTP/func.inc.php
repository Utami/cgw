<?
//Random Number
function array_random_assoc($arr, $num = 1) {
    $keys = array_keys($arr);
    shuffle($keys);

    $r = array();
    for ($i = 0; $i < $num; $i++) {
        $r[$i] = $arr[$keys[$i]];
    }
    return $r;
}

function random_by_addition($a,$c = 0) {
        if ($c == 0) {
                srand((float)microtime()*1000000);
                $c=rand();
        }

        $jml = (int)$a + (int)$c;
        $jml = "".$jml;
        while (strlen($jml) > 1 ) {
                $jumlah = 0;
                for ($i = 0;$i < strlen($jml);$i++ ) {
                        $jumlah = $jumlah + $jml[$i];
                }
                $jml = "".$jumlah;
        }
        return $jml;
}

function RandNumber() {
    $rv = array('1','2','3','4','5','6','7','8','9','0');
    $rv = array_random_assoc($rv, count($rv));

    $length = rand(1,count($rv)-1);
    for ($i = 0;$i <= $length;$i++ ) {
        $randArr[$i] = random_by_addition($rv[$i]);
    }
    $rv = array_merge($rv ,$randArr );
    $rv = array_random_assoc($rv, count($rv));
    return $rv[0];
}
/*
function RandNumber() {
    $rv = array('1','2','3','4','5','6','7','8','9','0');
    srand((float)microtime()*1000000);
    return $rv[array_rand($rv,1)];
}
*/
function Alpabetic() {
    $rv = array('1','2','3','4','5','6','7','8','9','0',
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',
        'p','q','r','s','t','u','v','w','x','y','z',
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
        'P','Q','R','S','T','U','V','W','X','Y','Z'
    );
    return $rv;
}

function RandAlpabet() {
    $rv = Alpabetic();
    srand((float)microtime()*1000000);
    return $rv[array_rand($rv,1)];
}

//generate replycode
function ReplyCode()
{
    $a = date('YmdHis');
    $c = GetMyMicrotime2();
    $b = RandNumber().RandNumber().RandNumber();
    $a = base64_encode("$a$c$b");
    $a = str_replace("=","",$a);
    $s = RandAlpabet();
    $r = RandAlpabet();
    $ret = trim("$s$a$r");
    return "$ret";
}

//getmicrotime2
function GetMyMicrotime2() {
    $c = "1000000";
    while (strlen($c) > 6) {
        $b = explode(" ",microtime());
        $c = round($b['0']*1000000);
    }
    if (strlen($c) == "0") $c = "000000";
    elseif (strlen($c) == "1") $c = "00000"."$c";
    elseif (strlen($c) == "2") $c = "0000"."$c";
    elseif (strlen($c) == "3") $c = "000"."$c";
    elseif (strlen($c) == "4") $c = "00"."$c";
    elseif (strlen($c) == "5") $c = "0"."$c";
    return $c;
}

//getmicrotime3
function GetMyMicrotime3() {
    $c = "1000000";
    while (strlen($c) > 4) {
        $b = explode(" ",microtime());
        $c = round($b['0']*10000);
    }
    if (strlen($c) == "0") $c = "0000";
    elseif (strlen($c) == "1") $c = "000"."$c";
    elseif (strlen($c) == "2") $c = "00"."$c";
    elseif (strlen($c) == "3") $c = "0"."$c";
    return $c;
}

// Check From Number
function CheckFromNumber($msisdn)
{
    if (preg_match('/^[0-9]+$/',$msisdn,$match) && (strlen($msisdn) > 9)) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}
// End Check From Number   

// Check From Number
function CheckFromNumberPush($msisdn)
{
        if (preg_match('/^[0-9]+$/',$msisdn,$match) && (strlen($msisdn) > 9)) {
            return TRUE;
        }
        else {
        return FALSE;
        }
}

// Check Prefix of User's HP                                                                                                 
function CheckIsTruePrefixNumber($msisdn,$dbname = '')
{
        for ($i = 0 ;$i <4;$i++) {
                $msisdnTemp = substr($msisdn,0,$i + 4);
                $myquery = "SELECT id FROM `p-prefix` "; 
                $myquery .= "WHERE `number` = '".addslashes($msisdnTemp)."' ";
				$result	= runsql($myquery,"mobile_services");
                if ($result) {
                    if ($row = mysql_fetch_array($result)) {
                        mysql_free_result($result);
                        return true;
                    }
                    mysql_free_result($result);
                }
        }
		return false;
}

//get parameter corporate
function GetValCoorporate($valIn){

        if (!(is_array($valIn))) {
            $valIn = array(
                'index_id' => '',
                'value_id' => '',
                'table_name' => '',
                'name_id' => ''
            );
        }
        $row = array(
                'id' => '',
                'group_send' => '',
                'name' => '',
                'db-name_var' => '',
                'db-queue_var' => ''
        );
        $ret = array(
                'coorporate_id' => '',
                'group_send' => '',
                'coorporate_name' => ''
        );
        
        $id = $valIn['index_id'];
        $myquery  = "SELECT `".addslashes($id)."` FROM `".addslashes($valIn['table_name'])."` ";
        $myquery .= "where `".addslashes($valIn['name_id'])."` = '".addslashes($valIn['value_id'])."' ";
        $result = runsql("$myquery", "db_client");
        if ($result) { 
            $row = mysql_fetch_array($result);
            $ret['coorporate_id'] = $row[$id];
            mysql_free_result($result);
        } 

        //$myquery  = "SELECT `db-queue_var`,`db-name_var`,name,group_send FROM `g_corporate` ";
		$myquery  = "SELECT `db-queue_var`,`db-name_var`,name,group_send, url_client_delivery_report, url_client_status_send_report, ";
		$myquery .= "delivery_report_parameters, status_send_parameters, optional_client_parameters  FROM `g_corporate` ";
        $myquery .= "where `id` = '".addslashes($ret['coorporate_id'])."' ";
        $result = runsql("$myquery","db_client");
        if ($result) {
            $row=mysql_fetch_array($result);
            mysql_free_result($result);
        }
   		$ret['coorporate_name'] = $row['name'];
        $ret['group_send'] = $row['group_send'];
        $ret['db-queue_var'] = $row['db-queue_var'];
        $ret['db-name_var'] = $row['db-name_var'];
		$ret['url_client_delivery_report'] = $row['url_client_delivery_report'];
		$ret['url_client_status_send_report'] = $row['url_client_status_send_report'];
		$ret['delivery_report_parameters'] = $row['delivery_report_parameters'];
		$ret['status_send_parameters'] = $row['status_send_parameters'];
		$ret['optional_client_parameters'] = $row['optional_client_parameters'];
        return $ret;
}
// Check Token
function GetTokenValue() {
        $tps = "0"; // TOKEN PER SMS
        $sql = "SELECT value FROM def_define WHERE name = 'token_per_sms' ";
        $res = runsql($sql,"db_client");
        if ($res) {
            if ($row = mysql_fetch_array($res)) {
                $tps = $row['value'];
            }
            mysql_free_result($res);
        }
        $tpsp = "0"; // TOKEN PER SENDER PERSONAL
        $sql = "SELECT value FROM def_define WHERE name = 'token_per_sender_personal' ";
        $res = runsql($sql,"db_client");
        if ($res) {
            if ($row = mysql_fetch_array($res)) {
                $tpsp = $row['value'];
            }
            mysql_free_result($res);
        }
        $ret['tps'] = $tps;
        $ret['tpsp'] = $tpsp;
        return $ret;
}
?>
