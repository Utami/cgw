<?php
require_once('lib/nusoap.php');
$client = new nusoap_client("http://172.17.0.164:9007/msgws.php?wsdl", true);

$error = $client->getError();
if ($error) {
    echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
}

$param = array(
    "JenisSMS" => '1',
    "KodeFitur" => '2',
    "TimeSend" => date('YmdHis'),
    "FlagDebet"=> '1',
    "FlagReply"=> '1',
    "ReffNo"   => '001',
    "MSISDN"   => '6285358399879',
    "SMSBody"  => 'dummy message',
);
$result = $client->call('WSSendSMS', $param);

if ($client->fault) {
    echo "<h2>Fault</h2><pre>";
    print_r($result);
    echo "</pre>";
} else {
    $error = $client->getError();
    if ($error) {
        echo "<h2>Error</h2><pre>" . $error . "</pre>";
    } else {
        echo "<h2>Result</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
}

echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug()) . '</pre>';
