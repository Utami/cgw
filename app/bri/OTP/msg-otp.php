<?PHP

error_reporting(NULL);

include "/var/www/bin/app/config.inc.php";
include "/var/www/bin/app/config_strip.php";
include "/var/www/bin/app/func.inc.php";
include "func.baru.php";

runConnectBRI($dbhost105);

// 0 is OK
$err1 = "Invalid Parameter";
$err2 = "Internal Error";
$err3 = "Invalid MSISDN";
$err4 = "Invalid Username/Password";
$err5 = "Membership is not available / not active";
$err6 = "IP is not in whitelist";
$err7 = "Not enough credit";
$err8 = "Invalid Sender Name";


$medianameGet  	= "client";
$replyCode = ReplyCode();

if (isset($_REQUEST['u'])) $username        = $_REQUEST['u'];else $username = "";
if (isset($_REQUEST['p'])) $passwd          = $_REQUEST['p'];else $passwd = "";
if (isset($_REQUEST['d'])) $dest            = trim($_REQUEST['d']);else $dest = "";

if (isset($dest['0'])) {
        if ($dest['0'] == "0") $dest = "62" . substr($dest,1,strlen($dest));
}
$dest = str_replace("+","",$dest);
$dest = str_replace(" ","",$dest);

// Testing Purpose
$arrTesting = array(
    "62811141449",   // Permata
    "628129964687"   // Sprint
);

if (isset($_REQUEST['m'])) $sms     = trim($_REQUEST['m']);else $sms = "";
if (isset($_REQUEST['s'])) $sender  = $_REQUEST['s'];else $sender = "";

$dateTimeNow	= date('YmdHis');
$ip_f = "";
if ($ip_f == "") $ip_f = getenv("http_client_ip");
if ($ip_f == "") $ip_f = getenv("HTTP_X_FORWARDED_FOR");
if ($ip_f == "") $ip_f = getenv("HTTP_X_FORWARDED");
if ($ip_f == "") $ip_f = getenv("HTTP_FORWARDED_FOR");
if ($ip_f == "") $ip_f = getenv("HTTP_FORWARDED");

$ip_r				= getenv('REMOTE_ADDR');
$server_port		= getenv('SERVER_PORT');
$deny = FALSE;$tokenUnlimited = FALSE;$tokenCoorp = FALSE;

if (($server_port == "8085") or ($server_port == "9006")) $modelService	= "http";
elseif ($server_port == "10082") $modelService	= "smpp";
else $deny = TRUE;

//paid by corporate
$mode_paid = "0"; 

if (($username == "") || ($sms == "") || ($dest == "") || ($passwd == "")) $deny = TRUE;

if ($deny) {
     //Invalid Parameter
     $code =  "1";
     $msgID = date('YmdHis').GetMyMicrotime3();
     $deny = true;
}

if (!(CheckFromNumber($dest))) {
        //MSISDN is not In WhiteList.";
	$code =  "3";
        $msgID  =  date('YmdHis').GetMyMicrotime3();
	$deny = true;
}

if (!(CheckIsTruePrefixNumber($dest))) {
        //MSISDN is not In Whitelist.";
        $code  =  "3";
	$msgID =  date('YmdHis').GetMyMicrotime3();
	$deny  = true;
}

// Mobile-8 Conversion
if (substr($dest,0,5) == "62888") {
 $dest = "0".substr($dest,2);
}

//Check Membership
if (!($deny)) {
	$sql  = "SELECT id,status,`i-tcp-user` as user,`i-tcp-pass` AS pass, ";
	$sql .= "`token_type`,`i-tcp-ip_private`,`i-tcp-ip_public`,`db-name_var` ,`db-queue_var` FROM `g_corporate` WHERE `i-tcp-user` = '$username' ";
	$res = runsql($sql,"db_client");
	if ($res) {
		if ($row = mysql_fetch_array($res)) {
			$coorpId = $row['id'];
			$valueCheck['table_name']       = "g_corporate";
			$valueCheck['index_id']         = "id";
			$valueCheck['name_id']          = "id";
			$valueCheck['value_id']         = $coorpId;
			$coorpVal = GetValCoorporate($valueCheck);


			$dbname1 = $row['db-name_var'];
			$dbname2 = $row['db-queue_var'];
			
			if (($row['db-name_var'] == "") OR ($row['db-name_var'] == "")) {
			        //Internal Error
			        $code =  "2";
				$msgID  =  date('YmdHis').GetMyMicrotime3();
				$deny = TRUE;
			}
			else {
			   
			}
			if ($row['token_type'] == "u" ) $tokenUnlimited = TRUE;
	
			if (!($deny)) {
    			    if (!($row['pass'] == $passwd)) { 
    			                //Invalid Username/Password
    			                $code  =  "4";
					$msgID =  date('YmdHis').GetMyMicrotime3();
					$deny=TRUE;
    			    }
			}
			if (!($deny)) {	
    			    if (!($row['status'] == "a")) {
    			        //Membership is Not Active
    			        $code =  "5";
		    		$msgID =  date('YmdHis').GetMyMicrotime3();
	    			$deny = TRUE;
    			    }
			}
			if (!($deny)) {	
    			    if ($row['i-tcp-ip_public'] != "") {
				if ($row['i-tcp-ip_public'] != $ip_r) {
				    //IP is not in whitelist
				    $code =  "6";
		    		    $msgID =  date('YmdHis').GetMyMicrotime3();
	    			    $deny = TRUE;
				}
    			    }
			}
			
		}
		else {
		        
		        //Invalid Username/Password
                        $code =  "4";
			$msgID =  date('YmdHis').GetMyMicrotime3();
			$deny=TRUE;
    		}
	}
	else {
	        //Invalid Username/Password
	        $code =  "4";
		$msgID =  date('YmdHis').GetMyMicrotime3();
		$deny=TRUE;
	}
}
// End Check Membership


///Check Token
if (!($deny)) {
	if (!($tokenUnlimited)) {	
	    $sql = "SELECT `id`,`rest` FROM `t-token_sms` WHERE `dl-name_id`='id-c' AND `dv-name_id`= '$coorpId' ";
	    $res = runsql($sql,"mobile_client_0002");
	    if ($res) {
			if ($row = mysql_fetch_array($res)) {
		    	$tokenRest = $row['rest'];
		    	$tokenId   = $row['id'];
		    	if (!($tokenRest + 1  > 1 )) {
		    	      //Not Enough Credit
		    	      $code =  "7";
			      $msgID =  date('YmdHis').GetMyMicrotime3();
			      $deny=TRUE;
		    	}
			}
			else $deny = TRUE; 
	    }
	    else $deny = TRUE;
	}
}
// End Check Token


// Check and Set Sender
if (!($deny)) {
	$senderName = "";
	if ($sender != "") {
		$sql2 = "SELECT `name` FROM `g_sender` WHERE `id-c` = '".$coorpId."' ";
		$sql2 .= "AND `sender_type` = 'share' ";
		$sql2 .= "AND `name` = '$sender' ";		
		$res2 = runsql($sql2,"db_client");
		if ($res2) {
			if ($row2= mysql_fetch_array($res2)) {
			    $senderName = $row2['name'];
			}
		}	
	}

	if ($senderName == "") {
		$sql2 = "SELECT `name` FROM `g_sender` WHERE `id-c` = '".$coorpId."' ";
		$sql2 .= "AND `sender_type` = 'share' ";
		$sql2 .= "AND `default_share` = 'y' ";
		$res2 = runsql($sql2,"db_client");
		if ($res2) {
			if ($row2= mysql_fetch_array($res2)) {
			    $senderName = $row2['name'];
			}
		}	
	}
	if ($senderName ==  "") {
	        //Invalid Sender Name
	        $code =  "8";
		$msgID =  date('YmdHis').GetMyMicrotime3();
		$deny = TRUE;
	}
}
// End Check Sender

if (!($deny)) {
    if (!($tokenUnlimited)) {
        $retToken = GetTokenValue();
	if (isset($retToken)) {
	    if ($retToken['tps'] != "") $tps = $retToken['tps'];
		else {
		    //Internal Error
		    $code =  "2";
		    $msgID = date('YmdHis').GetMyMicrotime3();
		    $deny = true;
		}
	    
	    }
	    else {
		//Internal Error
		$code =  "2";
	        $msg = date('YmdHis').GetMyMicrotime3();
		$deny = true;	    
	    }
	    if (!($deny)) {
    		$myquery  = "UPDATE `t-token_sms` set ";
    	        $myquery .= "rest =  rest - $tps ";
    		$myquery .= "WHERE id = '$tokenId'";
    	          $res = runsql($myquery,"mobile_client_0002");
	    }
	}
	
	if (!($deny)) {		
		
	$valSend=array();
	$valSend['group_send']		= $coorpVal['group_send'];
	$valSend['sendconfig_id']	= "";
	$valSend['msisdn']		= $dest;
	$valSend['model_service']	= $modelService;	
	$retTemp = GetValSendConfig($valSend);
	$send_config = $retTemp['id']; //id-p-s-cfg	
	$type_send = $retTemp['type_send'];
	$ids['have_own_qsend']		= $retTemp['have_own_qsend'];
	
	$recvDate = date('Y-m-d H:i:s');
	$tahun = substr($recvDate,0,4);
	$bulan = substr($recvDate,5,2);
	
	//Send to report
	$tableNameRecv = "i-".$tahun."_".$bulan."-smsrecv";
	$recvDate = date('Y-m-d H:i:s');	
	$retTemp = GetValServiceType($modelService);	    	
	if($retTemp['service_type_id'] != "0") {
		$retTemp = GetValSendConfig($valSend);
		$have_own_qsend = $retTemp['have_own_qsend'];
	}else{
		$have_own_qsend = "n";
	}
	$thread_no=rand(0,6);
	
	$myquery = "INSERT INTO `".addslashes($tableNameRecv)."` (`id-c`, `name-c`, `user_name_tcp`, `id-st`, `name-st`, ";
	$myquery .= "`id-p-s-cfg`, `name-p-s-cfg`, `dest_number`, `model_service`, `ip_remote_addr`, `name-sender`, ";
	$myquery .= "`sms_send`, `code_sms`, `status_send`, `status_delivery`, `time_querecv`, `time_dbrecv`, ";
	$myquery .= "`time_sched`, `time_startsend`, `time_send`, `time_sent`, `time_delivery`) ";
	$myquery .= "VALUES ('".addslashes($coorpId)."','".addslashes($coorpVal['coorporate_name'])."', ";
	$myquery .= "'".addslashes($username)."','".addslashes("7")."','".addslashes($modelService)."', ";
	$myquery .= "'".addslashes($send_config)."','".addslashes($retTemp['prvd_sendname'])."', ";
	$myquery .= "'".addslashes($dest)."','".addslashes($modelService)."','".addslashes($ip_r)."', ";
	$myquery .= "'".addslashes($senderName)."','".addslashes($sms)."','".addslashes($replyCode)."','2', ";
	$myquery .= "'3',NOW(),NOW(),NOW(),NOW(),NOW(),NOW(),NOW())";
	
	$result = runsql($myquery,"mobile_reports");
	
	$lastRecvId = mysql_insert_id();
	//$lastRecvId = $lastRecvId + 1;


	
	
	$myquery  = "INSERT INTO `q-prior` ( ";
	$myquery .="`tahun`,`bulan`,`id-smsrecv`,`id-p-s-cfg`,`id-c`,`type_send`,`paid_by`, ";
	$myquery .="`medianame_get`,`dest_number`,`sender_name`,`sms_send`,`model_service`,`time_req`, ";
	$myquery .="`time_dbrecv`,`code_sms`,`thread_no`,`have_own_qsend`,`time_flag`)";
	$myquery .= "VALUES (";
	$myquery .= "'$tahun','$bulan','$lastRecvId','$send_config','".addslashes($coorpId)."', ";
	$myquery .= "'$type_send','".addslashes($mode_paid)."','".addslashes($medianameGet)."', ";
	$myquery .= "'".addslashes($dest)."','".addslashes($senderName)."','".addslashes($sms)."', ";
	$myquery .= "'".addslashes($modelService)."','".addslashes($dateTimeNow)."','$recvDate', ";
	$myquery .= "'".addslashes($replyCode)."','".addslashes($thread_no)."','$have_own_qsend','$recvDate')";
	
	$res = runsql($myquery,"mobile_trx");
	
	if (!$res) {
	        $code  = "2";
		$msgID = date('YmdHis').GetMyMicrotime3();
		$deny  = true;
	}
		
		
	}
}

if (!($deny)) {
    //success
    $code =  "0";
    $msgID = $replyCode;
    print $code."-".$msgID;
}else{
    print  $code."-".$msgID;
}


exit;

?>
