#!/usr/bin/php -q
<?php

require_once __DIR__."/Config/ChannelIntegration.php";
require_once __DIR__."/App/Model/Databases.php";
require_once __DIR__."/App/Utils/Loging.php";

$t_name = '360_to_client';
$uniqid     = uniqid();

$query = "SELECT `id`, `url`, `data` FROM `$t_name`";
if (isset($argv[1])) $query .= " WHERE `thread` = {$argv[1]}";
$query .= " LIMIT 10";

$db = new \App\Model\Databases;
$result = $db->query($t_name, $query);
$id_to_delete = "";
if ($result->num_rows > 0) {
	$log = new \App\Utils\Loging($STCLIENT_LOG);
	while($row = mysqli_fetch_assoc($result)) {
		$ch = curl_init($row['url']);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $row['data']);
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
		$result_ch = curl_exec($ch);
		curl_close($ch);
		$log->write("REQUEST", "{$row['url']}->{$row['data']}");
		$log->write("RESPONSE", json_encode($result_ch));
		if (!$result_ch) continue;
		$db->query($t_name, "DELETE FROM `$t_name` WHERE `id` = {$row['id']}", true);
	}
}

sleep(5);

?>
