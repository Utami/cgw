<?php
require_once __DIR__ . "/Config/config.php";
require_once __DIR__ . "/App/Model/FuncDB.php";
require_once __DIR__ . "/App/Utils/Loging.php";
require_once __DIR__ . "/App/Utils/ParsData.php";
require_once __DIR__ . "/App/Controller/dlvrFunc.php";
require_once __DIR__ . "/App/Controller/mainDB.php";

$json_request = json_decode(file_get_contents('php://input'), true);
$sendData = new \App\Controller\dlvrFunc;
$sendData->setData($json_request);
$sendData->sendWhatsapp();
?>