<?php
namespace App\Utils;

class ParsData{
	public static function generateTime($arrData) {
		$rs = "";
		$t = explode(".", $arrData);
		$rs = str_replace("T", " ", $t[0]);
		
		return $rs;
	}// end class generateTime
	
	public static function generateDlvr($arrData) {
		$rs = array();
		$tsent = self::generateTime($arrData["results"][0]["sentAt"]);
		$tdlvr = self::generateTime($arrData["results"][0]["doneAt"]);
		$rs = array(
			"error" => $arrData["results"][0]["error"]["name"],
			"status" => $arrData["results"][0]["status"]["id"],
			"groupName" => $arrData["results"][0]["status"]["groupName"],
			"name" => $arrData["results"][0]["status"]["name"],
			"description" => $arrData["results"][0]["status"]["description"],
			"timesent" => $tsent,
			"timedelivery" => $tdlvr,
			"messageId" => $arrData["results"][0]["messageId"],
			"to" => $arrData["results"][0]["to"],
			"pricemessage" => $arrData["results"][0]["price"]["pricePerMessage"],
			"priceidr" => $arrData["results"][0]["price"]["currency"]
		);
		
		return $rs;
	}// end class generateDlvr
	
	public static function generateTracking($arrData) {
		$rs = array(
			"error" => "",
			"status" => $arrData["notificationType"],
			"groupName" => "",
			"name" => "",
			"description" => "",
			"timesent" => date('Y-m-d H:i:s', $arrData["sendDateTime"]/1000),
			//"timedelivery" => date('Y-m-d H:i:s', $arrData["sendDateTime"]/1000),
			"timedelivery" => date('Y-m-d H:i:s'),
			"messageId" => $arrData["messageId"],
			"to" => $arrData["recipient"],
			"pricemessage" => "",
			"priceidr" => ""
		);
		
		return $rs;
	}// end class generateTracking
	
	public static function generateWhatsapp($arrData) {
		$rs = array(
			"error" => "",
			"status" => strtolower($arrData["statuses"][0]["status"]),
			"groupName" => "",
			"name" => "",
			"description" => "",
			"timesent" => date('Y-m-d H:i:s', $arrData["statuses"][0]["timestamp"]),
			"timedelivery" => date('Y-m-d H:i:s', $arrData["statuses"][0]["timestamp"]),
			"messageId" => $arrData["statuses"][0]["id"],
			"to" => $arrData["statuses"][0]["recipient_id"],
			"pricemessage" => "",
			"priceidr" => ""
		);
		
		return $rs;
	}// end class generateWhatsapp
}// end class ParsData
?>