<?php
namespace App\Utils;

class Loging {
	
	private $fp;
    private $newPrefix = array();

    public function write ($S, $D, $F) {
		$filename = $GLOBALS["APP_LOG"].$F.".log";
		$this->fp = fopen($filename, "a+");
        $prefix = "[".date('Y-m-d H:i:s')."] ";
        foreach ($this->newPrefix as $value) {
            $prefix .= $value;
        }
        $prefix .= "[".$this->padding($S, 15)."] ";
        fwrite($this->fp, $prefix.$D.PHP_EOL);
        if (isset($GLOBALS["echo"])) {
            if ((boolean)$GLOBALS["echo"])
                echo $prefix.$D.PHP_EOL;
        }
		fclose($this->fp);
    }

    public function padding ($s, $len) {
        $result = $s;
        $len_s = strlen($s);
        if ($len_s > $len) {
            $result = substr($s, 0, $len);
        } 
        else if ($len_s < $len) {
            $pad_space = $len - $len_s;
            for ($i=0; $i<$pad_space; $i++) {
                $result .= " ";
            }
        }
        return $result;
    }


}
?>