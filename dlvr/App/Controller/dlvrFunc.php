<?php

namespace App\Controller;
use \App\Utils\Loging as Loging;
use \App\Utils\ParsData as ParsData;
use \App\Controller\mainDB as mainDB;

class dlvrFunc {
	protected $logfile;
	protected $dlvrdt;
	protected $mDB;
	protected $parsVal;

	public function __construct () {
		$this->logfile = new Loging;
		$this->parsVal = new ParsData;
		$this->mDB = new mainDB;
	}

	public function setData($dt){ $this->dlvrdt = $dt; }// end setData
	public function getData(){ return $this->dlvrdt; }// end getData

	public function chunkedresult($data) {
		$cont_type = "";
		$http_status_string = "";$http_status="";
		$temp = explode("\r\n\r\n",$data);
		$countPars = count($temp);
		$doChunk = "0";
		if ($countPars > 1) {
			$ret['header'] = trim($temp[0]);
			if ((preg_match('/<html>(.*)/i',trim($ret['header']),$match)) AND (preg_match('/<body>(.*)/i',trim($ret['header']),$match))){
				$ret['body'] = $data;
			}// end if preg_match
			else {
				$doChunk = "1";
				$ret['body']   = str_replace($ret['header'],"",$data);
				$statusTemp = "";
				if (preg_match('/^HTTP\/1\.[0-1] ([0-9][0-9][0-9] .*)\\r\\n/', $ret['header'], $match)) {
					$statusTemp = $match[1];
				}elseif (preg_match('/^HTTP\/1\.[0-1] ([0-9][0-9][0-9] .*)/', $ret['header'], $match)) {
					$statusTemp = $match[1];
				}// end if preg_match
			}// end if else preg_match
			$status = explode(" ",trim($statusTemp));
			if (isset($status[0])){ $http_status = $status[0]; }else{ $http_status = ""; }
			if (isset($match[0])){ $http_status_string = trim($match[0]); }else{ $http_status_string = ""; }
			if (preg_match('/Content\\-Type:\\s+([a-zA-Z\/]*)\\r\\n/',$ret['header'],$match)) { $cont_type = $match[1]; }elseif (preg_match('/Content\\-Type:\\s+([a-zA-Z\/]*)/',$ret['header'],$match)) { $cont_type = $match[1]; }// end if preg_match
			if ((strlen($cont_type) != (strlen(str_replace("xml","",$cont_type)))) AND ($cont_type != "")) { $cont_type = "xml"; }
		}// end if countPars
		if ($doChunk == "0") {
			$body = $data;
			if (substr(trim($data),0,5) == "<?xml") { $cont_type = "xml"; }
			if (preg_match('/^<html>(.*)<\/html>$/i',trim($data),$match)) {
				if (strlen(trim("<html>".$match[1]."</html>")) == strlen(trim($data)) ) {
					if (preg_match('/<body>(.*)<\/body>/i',trim($data),$match)){ $body = $match[1]; }
				}// end if strlen
			}// end if preg_match
			$ret['body'] = trim($body);
			$ret['header'] = "";
		}// end if doChunk
		$ret['http_status'] = $http_status;
		$ret['http_status_string'] = $http_status_string;
		if ($cont_type == ""){ $cont_type = "text/html"; }
		$ret['cont_type'] = $cont_type;

		return $ret;
	}// end chunkedresult

	public function sendBackData($arrData, $urldata){
		$maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
		$data_string = $arrData;
		$ch = curl_init();
		$keepAlive=TRUE;

		if($keepAlive){
			$GLOBALS['chCurl']=$ch;
			$header[]="Cache-Control: max-age=0";
			$header[]="Connection: keep-alive";
			$header[]="Keep-Alive: 300";
			$header[]="Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
			curl_setopt($ch, CURLOPT_MAXCONNECTS, $maxConnect);  // limiting connections
			curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
		}
		if($maxtryconencttime == ''){
			switch($maxtime){
				case ($maxtime > '30'): $maxtryconencttime='30'; break;
				case ($maxtime > '25' && $maxtime <= '30'): $maxtryconencttime='25'; break;
				case ($maxtime > '20' && $maxtime <= '25'): $maxtryconencttime='20'; break;
				case ($maxtime > '15' && $maxtime <= '20'): $maxtryconencttime='15'; break;
				case ($maxtime > '10' && $maxtime <= '15'): $maxtryconencttime='10'; break;
				case ($maxtime > '5' && $maxtime <= '10'): $maxtryconencttime='5'; break;
				default:
					$maxtryconencttime='2';
			}// end switch
		}
		$url = $urldata;
		curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_string)));
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
		$result = curl_exec($ch);
		$cont_type="";$response="";
		if(curl_errno($ch) == "0"){
			$ret['error']=0;
			$temp=$this->chunkedresult($result);
			$response=$temp['body'];
		}else{
			$ret['error']=1;
			$ret['full_iod']=$url;
			$response="Error/Connection Failed";
		}
		$ret['full_iod']=$url;
		$ret['trx_date']=date('YmdHis');
		$ret['response']=trim($response);
		$ret['cont_type']=$cont_type;
		if(!($keepAlive)){ curl_close($ch); }
		return $ret;
	}// end sendBackData

	public function sendQueue () {
		$request = $this->getData();
		$GLOBALS["logname"] = "delivery";
		$this->logfile->write(__FUNCTION__, "DLVR : ".json_encode($request), $GLOBALS["logname"]);

		$ip_f = "";
		if ($ip_f == ""){ $ip_f = getenv("http_client_ip"); }
		if ($ip_f == ""){ $ip_f = getenv("HTTP_X_FORWARDED_FOR"); }
		if ($ip_f == ""){ $ip_f = getenv("HTTP_X_FORWARDED"); }
		if ($ip_f == ""){ $ip_f = getenv("HTTP_FORWARDED_FOR"); }
		if ($ip_f == ""){ $ip_f = getenv("HTTP_FORWARDED"); }
		$ip_r = getenv("REMOTE_ADDR");

		$parsDlvr = $this->parsVal->generateDlvr($request);
		$parsDlvr["ip_remote_addr"] = $ip_r;
		$parsDlvr["ip_remote_host"] = $ip_f;
		$this->mDB->setVal($parsDlvr);
		$this->mDB->insDelivery();
	}// end sendQueue

	public function sendTracking () {
		$request = $this->getData();
		$GLOBALS["logname"] = "tracking";
		$this->logfile->write(__FUNCTION__, "Tracking : ".json_encode($request), $GLOBALS["logname"]);

		$ip_f = "";
		if ($ip_f == ""){ $ip_f = getenv("http_client_ip"); }
		if ($ip_f == ""){ $ip_f = getenv("HTTP_X_FORWARDED_FOR"); }
		if ($ip_f == ""){ $ip_f = getenv("HTTP_X_FORWARDED"); }
		if ($ip_f == ""){ $ip_f = getenv("HTTP_FORWARDED_FOR"); }
		if ($ip_f == ""){ $ip_f = getenv("HTTP_FORWARDED"); }
		$ip_r = getenv("REMOTE_ADDR");

		$parsDlvr = $this->parsVal->generateTracking($request);
		$parsDlvr["ip_remote_addr"] = $ip_r;
		$parsDlvr["ip_remote_host"] = $ip_f;
		$this->mDB->setVal($parsDlvr);
		$this->mDB->insDelivery();
	}// end sendTracking

	public function sendWhatsapp () {
		$request = $this->getData();
		$GLOBALS["logname"] = "whatsapp";
		$this->logfile->write(__FUNCTION__, "Whatsapp : ".json_encode($request), $GLOBALS["logname"]);
		$prvd = $request["statuses"][0]["id"];
		$recipient_id = $request["statuses"][0]["recipient_id"];
		$status = strtolower($request["statuses"][0]["status"]);
		$timestamp = date('Y-m-d H:i:s', $request["statuses"][0]["timestamp"]);

		$q = 'select * from `i-'.date("Y").'_'.date("m").'-delivery-socmed` where `prvd_message_id`="'.$prvd.'" order by `id` limit 1';
		$this->mDB->setVal($q);
		$getData = $this->mDB->inqDelivery();
		if($getData != "failed"){
			if(substr_count($status, "sent") > 0){
				$stssent = array("sent" => "1", "unsent" => "0");
			}else{
				$ip_f = "";
				if ($ip_f == ""){ $ip_f = getenv("http_client_ip"); }
				if ($ip_f == ""){ $ip_f = getenv("HTTP_X_FORWARDED_FOR"); }
				if ($ip_f == ""){ $ip_f = getenv("HTTP_X_FORWARDED"); }
				if ($ip_f == ""){ $ip_f = getenv("HTTP_FORWARDED_FOR"); }
				if ($ip_f == ""){ $ip_f = getenv("HTTP_FORWARDED"); }
				$ip_r = getenv("REMOTE_ADDR");
				$parsDlvr = $this->parsVal->generateWhatsapp($request);
				$parsDlvr["ip_remote_addr"] = $ip_r;
				$parsDlvr["ip_remote_host"] = $ip_f;
				$this->mDB->setVal($parsDlvr);
				$this->mDB->insDeliveryWhatsapp();
			}
		}
		//sulam tambal prosess
		$url = "http://172.27.0.104:8081/api/Report/DeliveryStatus";
		$q2 = 'select `ref_id`,`prvd_message_id`,`time_delivery`,`time_startsend` from `i-'.date("Y").'_'.date("m").'-smsrecv-socmed` where `prvd_message_id`="'.$prvd.'" order by `id` limit 1';
		$this->mDB->setVal($q2);
		$getData2 = $this->mDB->sendStsDeliveryWa();
		if (!is_null($getData2)) { //jika data tidak kosong

			$this->logfile->write(__FUNCTION__, "[Update Status Dlvr to URL : ".$url."] ->".json_encode($getData2), $GLOBALS["logname"]);
			$status_dlvr = null;
			if ($status=='delivered') {
				$status_dlvr = 1;
			}else if ($status=='undelivered') {
				$status_dlvr = 2;
			}else if ($status=='pending' || $status=='sent') {
				$status_dlvr = 3;
			}else if ($status=='failed') {
				$status_dlvr = 4;
			}else if ($status=='unknown') {
				$status_dlvr = 6;
			}else if ($status=='opened') {
				$status_dlvr = 7;
			}else if ($status=='clicked') {
				$status_dlvr = 8;
			}else if ($status=='read') {
				$status_dlvr = 9;
			}else {
				$status_dlvr = 5;
			}
			$dat_arr = array(
				"ref_id" => $getData2['ref_id'],
				"status" => $status_dlvr,
				"channel" => "whatsapp",
				"recipient" => $recipient_id,
				"time_delivery" => ($getData2['time_delivery']=="0000-00-00 00:00:00")?$getData2['time_startsend']:$getData2['time_delivery'],
				"time_request" => ($getData2['time_delivery']=="0000-00-00 00:00:00")?$getData2['time_startsend']:$getData2['time_delivery']
			);
			$data_string = json_encode($dat_arr);
			// $log->write("REQUEST", "toSandeza : ".$data_string."");
			$maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			// curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
			$rs = curl_exec($ch);
			curl_close($ch);
			$this->logfile->write(__FUNCTION__, "[Update Status Dlvr] : Response -> ".$rs, $GLOBALS["logname"]);
		} //end jika data tidak kosong
		//sulam tambal prosess
	}// end sendWhatsapp

	public function dlvrWhatsapp () {
		$request = $this->getData();
		$GLOBALS["logname"] = "dlvrwhatsapp";
		$this->logfile->write(__FUNCTION__, "Whatsapp : ".json_encode($request), $GLOBALS["logname"]);
		//print_r($request);echo "\n";
		//send sts delivery to kataai
		$urltokata = "https://kanal.kata.ai/receive_message/669a4dba-63ea-4a91-b30b-c11564a1e847";
		$ststokata = json_encode($request);
		$hitstskata = $this->sendBackData($ststokata, $urltokata);
		$tmplog = "ref_id :".$request["statuses"][0]["id"].", Callback : url ".$urltokata." ".$ststokata.", Resp : ".$hitstskata["response"]."\n";
		$this->logfile->write(__FUNCTION__, "Whatsapp : ".$tmplog, $GLOBALS["logname"]);
		//end send sts delivery to kataai

		$prvd = $request["statuses"][0]["id"];
		$recipient_id = $request["statuses"][0]["recipient_id"];
		$status = strtolower($request["statuses"][0]["status"]);
		$timestamp = date('Y-m-d H:i:s', $request["statuses"][0]["timestamp"]);

		$q = 'select * from `i-'.date("Y").'_'.date("m").'-delivery-socmed` where `prvd_message_id`="'.$prvd.'" order by `id` limit 1';
		$this->mDB->setVal($q);
		$getData = $this->mDB->inqDelivery();
		if($getData != "failed"){
			if(substr_count($status, "sent") > 0){
				$stssent = array("sent" => "1", "unsent" => "0");
				/*// insert smssend
				$iSmsSend	= array(
					"code_sms"=>$getData[0]['code_sms'],"prvd_status_send"=>$stssent[$status],"prvd_message_id"=>$prvd,"f7_tid"=>$getData[0]['f7_tid'],
					"status_send"=>$stssent[$status],"time_send"=>$timestamp,"operator_prefix_db"=>$getData[0]['operator_prefix_db'],"id-p-s-cfg"=>$getData[0]['id-p-s-cfg'],
					"client_prefix_dbname"=>$getData[0]['client_prefix_dbname'],"client_code_serv"=>$getData[0]['client_code_serv'],
					"engine_name_recv"=>$getData[0]['engine_name_recv'],"ref_id"=>$getData[0]['ref_id'],"time_sent"=>$timestamp
				);
				$nmTblSmsSend	= "i-".$getData[0]['tahun']."_".$getData[0]['bulan']."-smssend-socmed";
				$insSmsSend		= array(
					"arrdata"	=> $iSmsSend,
					"tablename"	=> $nmTblSmsSend,
					"dbname"	=> $getData[0]['client_prefix_dbname'],
					"confdb"	=> $GLOBALS["DB_TRX_CONFIG"]
				);
				$this->mDB->setVal($insSmsSend);
				$this->mDB->insertData();
				// end insert smssend
				//setcallback
				$urlCallBack	= str_replace("\\", "", $getData[0]['url_client_delivery_report']);
				$arrCallBack	= array(
					"ref_id"	=> $getData[0]['ref_id'],
					"code_sms"	=> $getData[0]['code_sms'],
					"type_status" => "send",
					"status"	=> $stssent[$status],
					"channel"	=> $getData[0]["operator_prefix_db"],
					"time_send"=> strtotime($getData[0]['time_send']),
					"time_prov_recv"=> strtotime($timestamp)
				);
				$dtCallBack = json_encode($arrCallBack);
				$hitCallBack = $this->sendBackData($dtCallBack, $urlCallBack);
				$tmplog		= "ref_id :".$getData[0]['ref_id'].", Callback : url ".$urlCallBack." ".$dtCallBack."\n";
				$this->logfile->write(__FUNCTION__, "Whatsapp : ".$tmplog, $GLOBALS["logname"]);*/
			}else{
				$ip_f = "";
				if ($ip_f == ""){ $ip_f = getenv("http_client_ip"); }
				if ($ip_f == ""){ $ip_f = getenv("HTTP_X_FORWARDED_FOR"); }
				if ($ip_f == ""){ $ip_f = getenv("HTTP_X_FORWARDED"); }
				if ($ip_f == ""){ $ip_f = getenv("HTTP_FORWARDED_FOR"); }
				if ($ip_f == ""){ $ip_f = getenv("HTTP_FORWARDED"); }
				$ip_r = getenv("REMOTE_ADDR");
				$parsDlvr = $this->parsVal->generateWhatsapp($request);
				$parsDlvr["ip_remote_addr"] = $ip_r;
				$parsDlvr["ip_remote_host"] = $ip_f;
				$this->mDB->setVal($parsDlvr);
				$this->mDB->insDeliveryWhatsapp();
			}
		}// end getData
	}// end dlvrWhatsapp

}// end class dlvrFunc
?>
