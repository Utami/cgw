<?php

namespace App\Controller;
use \App\Utils\Loging;
use \App\Utils\ParsData;
use \App\Utils\GetValue;
use \App\Utils\SendHttp;
use \App\Controller\Response;

class sendToQueue {
	protected $tipedata;
	protected $datasend;
	protected $prefix;
	protected $parsDataDB;
	protected $parsDataMQ;
	protected $severity;
	protected $router;
	protected $confmq;
	protected $response;
	protected $dataParamSms;
	protected $getVal;
	protected $logfile;
	protected $sendRetryTimeout;
	
	public function __construct () {
		$this->response		= new \App\Controller\Response;
		$this->logfile		= new \App\Utils\Loging;
		$this->dataParamSms	= new \App\Utils\ParsData;
		$this->getVal		= new \App\Utils\GetValue;
		$this->sendRetryTimeout	= new \App\Utils\SendHttp;
	}
	public function setTipeData($dt){ $this->tipedata = $dt; }// end setTipeData
	public function getTipeData(){ return $this->tipedata; }// end getTipeData
	public function setDataSend($dt){ $this->datasend = $dt; }// end setTipeData
	public function getDataSend(){ return $this->datasend; }// end getTipeData
	
	public function sendQueue () {
		$tpdt	= $this->getTipeData();
		$request= $this->getDataSend();
		if (isset($request["ref_id"])){ $GLOBALS["ref_id"] = $request["ref_id"]; }
		
		$GLOBALS["logname"] = strtoupper($tpdt)."_".strtoupper(str_replace(" ", "", $request["client_name"]));
		$this->logfile->write(__FUNCTION__, "REQUEST=".json_encode($request), $GLOBALS["logname"]);
		
		$request["dateNow"]	= date('YmdHis');
		$request["recvDate"]= date('Y-m-d H:i:s');
		switch(strtolower($tpdt)){
			case "sms":
				switch($request["provider"]){
					case "tsel":
					case "isat":
					case "xl":
						$this->prefix = $request["provider"];
					break;
					default:
						$this->prefix = "other";
				}
				$this->parsDataDB	= $this->dataParamSms->parsSmsDB($request);
				$this->severity		= strtolower("x.".$request["servicetype"].".".$this->prefix);
				$request["severity"]= $this->severity;
				$this->parsDataMQ	= $this->dataParamSms->parsSmsMq($request);
				$this->router		= "x.recv";
				$this->confmq		= $GLOBALS["CONF_MQ_SMS"];
			break;
			case "email":
				$this->prefix		= "email";
				$this->parsDataDB	= $this->dataParamSms->parsEmailDB($request);
				$this->severity		= strtolower("x.".$request["servicetype"].".email");
				$request["severity"]= $this->severity;
				$this->parsDataMQ	= $this->dataParamSms->parsEmailMq($request);
				$this->router		= "x.email";
				$this->confmq		= $GLOBALS["CONF_MQ_EMAIL"];
			break;
			case "socmed":
				$this->prefix		= "socmed";
				$this->parsDataDB	= $this->dataParamSms->parsSocmedDB($request);
				$this->severity		= strtolower("x.".$request["servicetype"].".socmed");
				$request["severity"]= $this->severity;
				$this->parsDataMQ	= $this->dataParamSms->parsSocmedMq($request);
				$this->router		= "x.socmed";
				$this->confmq		= $GLOBALS["CONF_MQ_SOCMED"];
			break;
		}// end switch
		$arrTbl		= array("thn"=>date("Y"),"bln"=>date("m"),"srvc"=>$request["servicetype"],"prefix"=>$this->prefix,"nmtbl"=>"smsrecv");
		$nmTable	= $this->getVal->getNameTable($arrTbl);
		$arrInsert	= array(
			"arrdata"	=> $this->parsDataDB,
			"tablename"	=> $nmTable,
			"dbname"	=> $request["client_name"]
		);
		$jsondata	= $this->dataParamSms->CreateJson($this->parsDataMQ);
		$arrInMq	= array(
			"router"	=> $this->router,
			"severity"	=> $this->severity,
			"jsonmq"	=> $jsondata,
			"confmq"	=> $this->confmq
		);
		$dataTmp	= array("datamq" => $arrInMq, "datadb" => $arrInsert, "logname" => $GLOBALS["logname"]);
		$dataInsert	= json_encode($dataTmp);
		$urlinsert	= $GLOBALS["URLINSERT"].strtolower($tpdt);
		$insData	= $this->sendRetryTimeout->sendBackData($dataInsert, $urlinsert);
		$tmplog		= "Resp Insert :".$insData['response'];
		$this->logfile->write(__FUNCTION__, $tmplog, $GLOBALS["logname"]);
		
		$this->response->SUCCESS();
	}// end sendMqSms
}
?>