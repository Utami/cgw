<?php
namespace App\Controller;
use \App\Model\FuncDB;

class mainDB {
	protected $dt;
	protected $tblqueue;

	public function setVal($dt){ $this->dt = $dt; }
	public function getVal(){ return $this->dt; }

	public function insDelivery(){
		$rs = false;
		$valdata = $this->getVal();
		$logfile = new \App\Utils\Loging ;
		$db	= new \App\Model\FuncDB;
		$conn = $db->connectDb("mobile_trx", $GLOBALS["DB_TRX_CONFIG"]);
		if($conn){
			$q = "INSERT INTO `q-deliveryfromoperator-email` SET `prvd_message_id`='".$valdata["messageId"]."', ";
			$q.= "`prvd_status_delivery`='".$valdata["status"]."', `prvd_status_other`='".$valdata["name"]."', ";
			$q.= "`prvd_message_id_real`='".$valdata["description"]."', `prvd_status_delivery_real`='".$valdata["error"]."', ";
			$q.= "`prvd_status_delivery_err_real`='".$valdata["pricemessage"]." ".$valdata["priceidr"]."', ";
			$q.= "`time_delivery`='".$valdata["timedelivery"]."', `ip_remote_addr`='".$valdata["ip_remote_addr"]."', `ip_remote_host`='".$valdata["ip_remote_host"]."'";
			$s = $db->queryDB($q, $conn);
			if(!$s){ $logfile->write(__FUNCTION__, "Failed Insert : ".$q, $GLOBALS["logname"]); }
			$db->closeDB($conn);
		}else{ $logfile->write(__FUNCTION__, "Failed Connect DB", $GLOBALS["logname"]); }
	}//end insDelivery

	public function insDeliveryWhatsapp(){
		$rs = false;
		$valdata = $this->getVal();
		$logfile = new \App\Utils\Loging ;
		$db	= new \App\Model\FuncDB;
		// $conn = $db->connectDb("mobile_trx", $GLOBALS["DB_TRX_CONFIG"]);
		$conn = $db->connectDb("cidx", $GLOBALS["DB_TRX_CONFIG"]);
		if($conn){
			$q = "INSERT INTO `q-deliveryfromoperator-socmed` SET `prvd_message_id`='".$valdata["messageId"]."', ";
			$q.= "`prvd_status_delivery`='".$valdata["status"]."', `prvd_status_other`='".$valdata["name"]."', ";
			$q.= "`prvd_message_id_real`='".$valdata["description"]."', `prvd_status_delivery_real`='".$valdata["error"]."', ";
			$q.= "`prvd_status_delivery_err_real`='".$valdata["pricemessage"]." ".$valdata["priceidr"]."', ";
			$q.= "`time_delivery`='".$valdata["timedelivery"]."', `ip_remote_addr`='".$valdata["ip_remote_addr"]."', `ip_remote_host`='".$valdata["ip_remote_host"]."'";
			$s = $db->queryDB($q, $conn);
			if(!$s){ $logfile->write(__FUNCTION__, "Failed Insert : ".$q, $GLOBALS["logname"]); }
			$db->closeDB($conn);
		}else{ $logfile->write(__FUNCTION__, "Failed Connect DB", $GLOBALS["logname"]); }
	}//end insDeliveryWhatsapp

	public function inqDelivery(){
		$rs = "failed";
		$valdata = $this->getVal();
		$logfile = new \App\Utils\Loging ;
		$db	= new \App\Model\FuncDB;
		$conn = $db->connectDb("mobile_trx", $GLOBALS["DB_TRX_CONFIG"]);
		if($conn){
			$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$db->queryDB($qset, $conn);
			$s = $db->queryDB($valdata, $conn);
			if(@$s->num_rows > 0){
				$rs = array();
				$r = $db->rowDB($s);
				foreach($r as $v){
					$parsarr = array();
					foreach($v as $kdt => $vdt){
						if(!is_numeric($kdt)){
							$tmp = array($kdt => $vdt);
							$parsarr = array_merge($parsarr, $tmp);
						}
					}
					array_push($rs, $parsarr);
				}
			}
			$db->closeDB($conn);
		}

		return $rs;
	}//end inqDelivery

	public function sendStsDeliveryWa(){

		$valdata = $this->getVal();
		$logfile = new \App\Utils\Loging ;
		$db	= new \App\Model\FuncDB;
		$dat = null;
		$conn = $db->connectDb("cidx", $GLOBALS["DB_TRX_CONFIG"]);
		if($conn){
			$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$db->queryDB($qset, $conn);
			$s = $db->queryDB($valdata, $conn);
			if(@$s->num_rows > 0){
				$dat = $s->fetch_assoc();
			}
		}

		return $dat;
	}

	public function insertData(){
		$rs = "failed";
		$valdata = $this->getVal();
		$logfile = new \App\Utils\Loging ;
		$db	= new \App\Model\FuncDB;
		$conn = $db->connectDb("cidx", $GLOBALS["DB_TRX_CONFIG"]);
		if($conn){
			$qset = "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$db->queryDB($qset, $conn);
			$query = "";
			foreach($valdata["arrdata"] as $key => $val){
				$query	.= "`".$key."`='".addslashes($val)."', ";
			}// end foreach
			$query	= substr($query, 0, -2);
			$q		= "INSERT INTO `".$valdata["tablename"]."` SET ".$query;
			$inDB	= $db->queryDB($q, $conn);
			if($inDB){ $rs = "ok"; }
			$db->closeDB($conn);
		}

	}// end insertData
}// end class
?>
